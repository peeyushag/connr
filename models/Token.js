var Mongoose = require('mongoose');

var TokenSchema = new Mongoose.Schema({
	token : { type : String, required : false },
	userId : { type : String, required : false },
    });

exports.TokenSchema = TokenSchema;