var validate = require('mongoose-validate');
var Mongoose = require('mongoose');

exports.StoreSchema = new Mongoose.Schema({
	name : { type : String, required : true },
	desc : { type : String, required : false },
	created_at : { type : Date, required : true, default : Date.now },
	deleted_at : { type : Date, required : false },
	//revenues : { type : Number, required : false },
	address : {
	    street1 : { type : String, required : false },
	    street2 : { type : String, required : false },
	    city : { type : String, required : false },
	    state : { type : String, required : false },
	    country : { type : String, required : false },
	    zip : { type : Number, required : false,  validate: [validate.postalCode, 'invalid email address'] }
	},
	phone: { type: String, required: true },
	email: {type: String, required: true, validate: [validate.email, 'invalid email address']},
	loc: { type : [], required : true },
	is_active: { type: Boolean, default: true },
	offers: { type : [] }
    });

exports.StoreSchema.index({loc : '2d'});
