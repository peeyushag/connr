var Mongoose = require('mongoose');

exports.ClickObjectSchema = new Mongoose.Schema({
	user_id: {
	    type : String,
	    required : true
	},
	geo: {
	    type: [],
	    required: true
	},
	time: {
	    type: Date,
	    required:true
	},
    });

exports.ClickObjectSchema.index({geo: '2d'});
