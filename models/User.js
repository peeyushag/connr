var Mongoose = require('mongoose');
var hash = require('../utils/hash');
var crypto = require('crypto');

var UserSchema = new Mongoose.Schema({
	firstName:  { type : String, required : false },
	lastName:   { type : String, required : false },
	username:   { type : String, required : false },
	salt:       { type : String },
	password:   { type : String },
	facebook:{
	    id:       String,
	    email:    String,
	    name:     String
	},
	google:{
	    id:       String,
	    email:    String,
	    name:     String
	},
	subscribed: { type : [] }
    });

// TODO DO not throw an error here. Bad idea. Return false instead.
UserSchema.statics.findOrCreateLocalUser = function(username, password, done) {
    var User = this;
    User.findOne({username : username}, function(error, user) {
	    if (!error && user) {
		if (!validatePassword(password, user.password)) {
		    return done(error);
		}
		done(null, user);
	    } else {
		saltAndHash(password, function(hash) {
			user = {
			    username : username,
			    password : hash
			};

			User.create(user,
				    function(err, user){
					if(err) throw err;
					done(null, user);
				    });
		    });
	    }
	});
};

// Create a new user given a profile
UserSchema.statics.findOrCreateOAuthUser = function(profile, done) {
    var User = this;

    // Build dynamic key query
    var query = {};
    query[profile.authOrigin + '.id'] = profile.id;

    // Search for a profile from the given auth origin
    User.findOne(query, function(err, user){
	    if(err) throw err;

	    // If a user is returned, load the given user
	    if(user){
		done(null, user);
	    } else {
		// Otherwise, store user, or update information for same e-mail
		User.findOne({ 'email' : profile.emails[0].value }, function(err, user){
			if(err) throw err;

			if(user){
			    // Preexistent e-mail, update
			    user[''+profile.authOrigin] = {};
			    user[''+profile.authOrigin].id = profile.id;
			    user[''+profile.authOrigin].email = profile.emails[0].value;
			    user[''+profile.authOrigin].name = profile.displayName;

			    user.save(function(err, user){
				    if(err) throw err;
				    done(null, user);
				});
			} else {
			    // New e-mail, create

			    // Fixed fields
			    user = {
				email : profile.emails[0].value,
				firstName : profile.displayName.split(" ")[0],
				lastName : profile.displayName.replace(profile.displayName.split(" ")[0] + " ", "")
			    };

			    // Dynamic fields
			    user[''+profile.authOrigin] = {};
			    user[''+profile.authOrigin].id = profile.id;
			    user[''+profile.authOrigin].email = profile.emails[0].value;
			    user[''+profile.authOrigin].name = profile.displayName;

			    User.create(
					user,
					function(err, user){
					    if(err) throw err;
					    done(null, user);
					});
			}
		    });
	    }
	});
}

exports.UserSchema = UserSchema;

var generateSalt = function() {
    var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
    var salt = '';
    for (var i = 0; i < 10; i++) {
        var p = Math.floor(Math.random() * set.length);
        salt += set[p];
    }
    return salt;
};

var md5 = function(str) {
    return crypto.createHash('md5').update(str).digest('hex');
};

var saltAndHash = function(pass, callback) {
    var salt = generateSalt();
    callback(salt + md5(pass + salt));
};

var validatePassword = function(plainPass, hashedPass) {
    var salt = hashedPass.substr(0, 10);
    var validHash = salt + md5(plainPass + salt);
    return hashedPass === validHash;
};
