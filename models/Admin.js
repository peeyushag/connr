var validate = require('mongoose-validate');
var Mongoose = require('mongoose');
var crypto = require('crypto');

var AdminSchema = new Mongoose.Schema({
	name : { type : String, required : false },
	post : { type : String, required : false },
	country : { type : String, required : false },
	auth : {
	    user : { type : String, required : true },
	    pass : { type : String, required : true }
	},
	created_at : { type : Date, required : true, default : Date.now },
	deleted_at : { type : Date, required : false },
	mobile : { type : Number, required : false },
	email : { type : String, required : true, validate: [validate.email, 'invalid email address'] },
	stores : { type : [] },
	permissions : { type : String, default : "ABCD" }, // Admins created via signup pages have all permissions
	subadmins : { type : [] },
    });

AdminSchema.statics.findAdmin = function(username, password, done) {
    var Admin = this;
    Admin.findOne({"auth.user" : username}, function(error, admin) {
	    if (!error && admin) {
		if (!validatePassword(password, admin.auth.pass)) {
		    return done(error);
		}
		return done(null, admin);
	    } else {
		return done(error);
	    }
	});
};

AdminSchema.statics.createAdmin = function(admin) {
    var Admin = this;

    Admin.create(admin,
		 function(err, admin) {
		     if (err) return false;
		     return true
		 });
};

exports.AdminSchema = AdminSchema;

var generateSalt = function() {
    var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
    var salt = '';
    for (var i = 0; i < 10; i++) {
        var p = Math.floor(Math.random() * set.length);
        salt += set[p];
    }
    return salt;
};

var md5 = function(str) {
    return crypto.createHash('md5').update(str).digest('hex');
};

var saltAndHash = function(pass, callback) {
    var salt = generateSalt();
    callback(salt + md5(pass + salt));
};

var validatePassword = function(plainPass, hashedPass) {
    var salt = hashedPass.substr(0, 10);
    var validHash = salt + md5(plainPass + salt);
    return hashedPass === validHash;
};
