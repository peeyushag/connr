var Mongoose = require('mongoose');

exports.OfferSchema = new Mongoose.Schema({
	name : {
	    type : String,
	    required : true
	},
	offer_type : {
	    type : String,
	    required : true
	},
	stores : {
	    type : [],
	    required : true
	},
	image : {
	    type : String
	},
	investment : {
	    type : String,
	    required : false
	},
	is_active : {
	    type : Boolean,
	default: true,
	    required : true
	},
	start : {
	    type : Date,
	    required : true
	},
	end : {
	    type : Date,
	    required : true
	},
	deleted_at : {
	    type : Date,
	    required : false
	},
	description : {
	    type : String,
	    required : true
	},
	product_type : {
	    type : String,
	    required : true
	},
	brands : {
	    type : String,
	    required : true
	},
	model : {
	    type : String,
	    required : true
	},
	num_recipients : {
	    type : []
	},
	click_objects : {
	    type : []
	}
    });
