/**
 * Environment
 */
var env = process.env.NODE_ENV;

/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var https =require('https');
var fs = require('fs');
console.log(env);
if (env == 'development') {
    var privateKey  = fs.readFileSync('./ssl/key.pem');
    var certificate = fs.readFileSync('./ssl/key-cert.pem');
} else {
    var privateKey  = fs.readFileSync('/home/ubuntu/ssl/key.pem');
    var certificate = fs.readFileSync('/home/ubuntu/ssl/key-cert.pem');
}

var credentials = {key: privateKey, cert: certificate};

var path = require('path');
var passport = require('passport');

var app = express();
var Mongoose = require('mongoose');
var db = Mongoose.createConnection('localhost', 'ConnR');

var OfferSchema = require('./models/Offer.js').OfferSchema;
var StoreSchema = require('./models/Store.js').StoreSchema;
var AdminSchema = require('./models/Admin.js').AdminSchema;
var UserSchema = require('./models/User.js').UserSchema;
var TokenSchema = require('./models/Token.js').TokenSchema;
var ClickObjectSchema = require('./models/ClickObject.js').ClickObjectSchema;
var Offer = db.model('offers', OfferSchema);
var Store = db.model('stores', StoreSchema);
var Admin = db.model('admin', AdminSchema);
var User = db.model('user', UserSchema);
var Token = db.model('token', TokenSchema);
var ClickObject = db.model('clickobject', ClickObjectSchema);

var config = require('./routes/config/config')[env];

require('./routes/config/passport')(passport, config);
var Auth = require('./routes/config/middleware/authorization');

var rndstr = require('./utils/rndstr');

// all environments
app.set('port', process.env.PORT || 3000);

if (env == 'development') {
    app.set('port_ssl', process.env.PORT || 3001);
} else {
    app.set('port_ssl', process.env.PORT_SSL || 443);
}

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.locals.pretty = true;
//app.use(express.favicon());
//app.use(express.logger('dev'));
app.configure(function() {
	app.use(express.json());
	//app.use(flash());
	app.use(express.methodOverride());
	app.use(express.cookieParser());
	app.use(express.bodyParser({
		keepExtensions: true,
		uploadDir: __dirname +'/temp' })); 
	app.use(express.cookieSession({
		    secret: 'keyboard cat',
			cookie: { maxAge : 60 * 60 * 1000
			    }}));
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(passport.authenticate('remember-me'));
	app.use(app.router);
	app.use(express.static('public'));
	app.use(require('stylus').middleware({ src: __dirname + 'public' })); //CSS preprocessor??
    });

app.all('*', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "http://localhost:8000");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, content-type");
	res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT");
	res.header('Access-Control-Allow-Credentials', true);
	console.log(res);
	next();
    });

app.use(app.router);
app.use(function(req, res) {
	res.redirect('/');
    });

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//Home Page - General API - for non service users
app.get('/',function(req,res){
        res.sendfile('./public/html/landingPage.html');
    });
app.get('/api/admin/general/login', Auth.isAdminAuthenticated, routes.index(Offer));

app.post("/upload", function (req, res) {
	console.log(JSON.stringify(req.files));
	//get the file name
	var filename=req.files.file0.name;
	var extensionAllowed=[".jpeg",".JPG",".png",".PNG"];
	var maxSizeOfFile=1000000;
	var msg="";
	var i = filename.lastIndexOf('.');
	// get the temporary location of the file
	var tmp_path = req.files.file0.path; 
	// set where the file should actually exists - in this case it is in the "images" directory
	var target_path = __dirname +'/public/images/' + req.files.file0.name; 
	var file_extension= (i < 0) ? '' : filename.substr(i); 
	console.log("File extension" + file_extension);
	if((file_extension in oc(extensionAllowed))&&((req.files.file0.size /1024 )< maxSizeOfFile)){ 
		fs.rename(tmp_path, target_path, function(err) {
	if (err) throw err; 
	// delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files 
		fs.unlink(tmp_path, function() {
	if (err) throw err;
		});
		});
		msg="File uploaded sucessfully" + file_extension + " "+JSON.stringify(oc(extensionAllowed)) + " "+ tmp_path +" "+ req.files.file0.size;
	}else{
		// delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files 
		fs.unlink(tmp_path, function(err) {
		if (err) throw err;
		});
		msg="File upload failed.File extension not allowed and size must be less than "+maxSizeOfFile; 
		}
		res.json(msg);
		//res.json("From the netherworlds:" + req.files);
});

	function oc(a){
		var o = {};
		for(var i=0;i<a.length;i++) {
		o[a[i]]='';
		}
	return o; 
	}
/**
   Routes for admin interaction

   All of these methods require a valid admin authentication.
   On top of that, we check the permissions of each admin to
   ensure they are authorized to perform certain operations.
 **/

/* Admin authentication */
app.get('/api/admin/general/signup', routes.signupAdminShow());
app.post('/api/admin/general/signup', routes.signupAdmin(Admin));

app.post('/api/admin/auth/local',
	 passport.authenticate('admin', {
		 failureRedirect: '/'}),
	 function(req, res, next) {
	     // issue a remember me cookie if the option was checked
	     if (!req.body.remember_me) { return next(); }
	     
	     var token = new Token();
	     token.tokenId = rndstr.randomString(64);
	     token.userId = req.user.id;
	     token.save(function(err) {
		     if (err) { return done(err); }
		     res.cookie('remember_me', token, { path: '/', httpOnly: false, maxAge: 604800000 }); // 7 days
		     return next();
		 });
	 },
	 function(req, res) {
	     res.redirect('/');
	 });

app.get('/api/admin/auth/logout', function(req, res){
	req.logout();
	res.clearCookie("remember_me");
	return res.json({success : "Logged out"});
    });

app.get('/api/admin/profile', Auth.isAdminAuthenticated, function(req, res) {
	return res.json("Welcome: " + req.user);
    });

app.get('/api/admin/settings', Auth.isAdminAuthenticated, routes.settings(Admin));

/* Store CRUD operations. Requires control panel authorization */
app.post('/api/admin/add_store',
	 Auth.isAdminAuthenticated,
	Auth.hasControlPanelPermissions,
	routes.addStore(Admin, Store));
app.get('/api/admin/show_stores',
	Auth.isAdminAuthenticated,
	Auth.hasControlPanelPermissions,
	routes.showStores(Store));
app.get('/api/admin/show_store',
	Auth.isAdminAuthenticated,
	Auth.hasControlPanelPermissions,
	Auth.hasStoreAuthorization,
	routes.showStore(Store));
app.post('/api/admin/update_store',
	 Auth.isAdminAuthenticated,
	 Auth.hasControlPanelPermissions,
	 Auth.hasStoreAuthorization,
	 routes.updateStore(Store));
app.post('/api/admin/delete_store',
	 Auth.isAdminAuthenticated,
	 Auth.hasControlPanelPermissions,
	 Auth.hasStoreAuthorization,
	 routes.deleteStore(Store));

/* Sub admin CRUD operations. Requires control panel authorization */
app.post('/api/admin/add_sub_admin',
	 Auth.isAdminAuthenticated,
	 Auth.hasControlPanelPermissions,
	 Auth.hasStoreAuthorization,
	 Auth.hasPermissionAuthorization,
	 routes.addSubAdmin(Admin));
app.get('/api/admin/show_sub_admins',
	Auth.isAdminAuthenticated,
	Auth.hasControlPanelPermissions,
	routes.showSubAdmins(Admin));
app.get('/api/admin/show_sub_admin',
	Auth.isAdminAuthenticated,
	Auth.hasControlPanelPermissions,
	Auth.hasAdminAuthorization,
	routes.showSubAdmin(Admin));
app.post('/api/admin/update_sub_admin',
	 Auth.isAdminAuthenticated,
	 Auth.hasControlPanelPermissions,
	 Auth.hasAdminAuthorization,
	 Auth.hasPermissionAuthorization,
	 routes.updateSubAdmin(Admin));
app.post('/api/admin/delete_sub_admin',
	 Auth.isAdminAuthenticated,
	 Auth.hasControlPanelPermissions,
	 Auth.hasAdminAuthorization,
	 routes.deleteSubAdmin(Admin));
app.post('/api/admin/add_permission',
	 Auth.isAdminAuthenticated,
	 Auth.hasControlPanelPermissions,
	 Auth.hasAdminAuthorization,
	 Auth.hasPermissionAuthorization,
	 routes.addAdminPermission(Admin));
app.post('/api/admin/remove_permission',
	 Auth.isAdminAuthenticated,
	 Auth.hasControlPanelPermissions,
	 Auth.hasAdminAuthorization,
	 Auth.hasPermissionAuthorization,
	 routes.removeAdminPermission(Admin));
app.post('/api/admin/add_to_store',
	 Auth.isAdminAuthenticated,
	 Auth.hasControlPanelPermissions,
	 Auth.hasAdminAuthorization,
	 Auth.hasStoreAuthorization,
	 routes.addAdminStore(Admin));
app.post('/api/admin/remove_from_store',
	 Auth.isAdminAuthenticated,
	 Auth.hasControlPanelPermissions,
	 Auth.hasAdminAuthorization,
	 Auth.hasStoreAuthorization,
	 routes.removeAdminStore(Admin)); 

/* KPI API calls */
//TODO

app.get('/api/admin/dashboard',
	Auth.isAdminAuthenticated,
	routes.index(Offer));
app.post('/api/admin/delete_account',
	 Auth.isAdminAuthenticated,
	 routes.delAdminAccount(Admin));
app.post('/api/admin/update_account',
	 Auth.isAdminAuthenticated,
	 routes.updateAdminAccount(Admin));
app.get('/api/admin/dTableOffers', routes.dataTableOffer(Offer));

/* Routes for store interaction */

/* Offers CRUD operations */
app.post('/api/admin/store/add_offer',
	 Auth.isAdminAuthenticated,   // Are you a valid admin?
	 Auth.hasOfferPermissions,    // Do you have the permissions to create offers?
	 Auth.hasStoreAuthorization,  // Are you authorized to create offers for this store?
	 routes.adminAddOffer(Offer, Store));
app.get('/api/admin/store/get_offer',
	Auth.isAdminAuthenticated,
	Auth.hasOfferPermissions,
	Auth.hasStoreAuthorization,
	routes.adminGetOffer(Offer));
app.get('/api/admin/store/get_offers', /* This function gets all the offers from all authorized stores*/
	Auth.isAdminAuthenticated,
	Auth.hasOfferPermissions,
	routes.adminGetOffers(Offer));
app.post('/api/admin/store/delete/',
	 Auth.isAdminAuthenticated,
	 Auth.hasOfferPermissions,
	 Auth.hasStoreAuthorization,
	 routes.deleteOffer(Offer));
app.post('/api/admin/store/modify/',
	 Auth.isAdminAuthenticated,
	 Auth.hasOfferPermissions,
	 Auth.hasStoreAuthorization,
	 routes.modifyOffer(Offer));

//app.get('/api/admin/store/kpi/overall', routes.getKpiOverall());


/**
   Routes for user interaction.

   These are all the methods that will be used to allow users to
   authenticate, browse offers, perform actions on them and in
   general, communicate with ConnR.
   All of these methods require a valid User authentication
 **/
/*
  User - Store interaction
*/
app.get('/api/user/subscribe',
	 Auth.isAuthenticated,
	 routes.storeSubscribe(Store));
app.get('/api/user/unsubscribe',
	 Auth.isAuthenticated,
	 routes.storeUnsubscribe(Store));
app.get('/api/user/store_info',
	Auth.isAuthenticated,
	routes.storeGetInfo(Store));
app.get('/api/user/get_subscribed_stores',
	Auth.isAuthenticated,
	routes.storeGetSubscribedStores(Store));
/*
  User - Store location services
*/
app.get('/api/user/get_stores_near',
	Auth.isAuthenticated,
	routes.storeGetNear(Store));
app.get('/api/user/get_stores_within',
	Auth.isAuthenticated,
	routes.storeGetWithin(Store));

/*
  User - Offer interaction
*/
app.get('/api/user/get_all_soffers',
	Auth.isAuthenticated,
	routes.storeGetAllOffers(Offer));
app.get('/api/user/get_active_soffers',
	Auth.isAuthenticated,
	routes.storeGetActiveOffers(Offer, Store));
app.get('/api/user/clicked_offer',
	 Auth.isAuthenticated,
	 routes.getUserClick(Offer, ClickObject));
app.get('/api/user/get_all_offers',
	Auth.isAuthenticated,
	routes.getAllOffers(Offer));
app.get('/api/user/get_active_offers',
	Auth.isAuthenticated,
	routes.getAllActiveOffers(Offer));

app.get('/api/user/offers', routes.initOffer(Offer));
app.get('/api/user/dTableOffers', routes.dataTableOffer(Offer));

/*
  User - Offer analytics
*/
app.post('/api/user/viewed_offer', routes.viewOffer(Offer));

/*
  Admin - Offer KPI
*/
app.get('/api/admin/offer/total_clicks', routes.getTotalClicks(Offer));

/*
  Top-k statistics
*/
app.get('/api/admin/offer/top_k_users', routes.getTopKUsers(Offer));
app.get('/api/admin/offer/top_k_geo', routes.getTopKGeo(Offer));
app.get('/api/admin/offer/top_k_times', routes.getTopKTimes(Offer));

/*
  User authentication mechanisms
  Implemented: Facebook, Google, Local strategy
 */
// Passport user authentication
app.get("/api/user/auth/facebook", passport.authenticate("facebook", {
	    scope : "email" }));

app.get("/api/user/auth/facebook/callback",
	passport.authenticate("facebook", {
		failureRedirect : '/notfound',
		    successRedirect: '/api/user/profile'}));

app.get('/api/user/auth/google', passport.authenticate('google', {
	    scope: ['https://www.googleapis.com/auth/userinfo.profile',
		    'https://www.googleapis.com/auth/userinfo.email']}));

app.get('/api/user/auth/google/callback', 
	passport.authenticate('google', {
		failureRedirect: '/api/user/login',
		    successRedirect: '/api/user/profile'}));

app.get('/api/user/auth/local',
	passport.authenticate('user', {
		failureRedirect: '/notfound'}),
	function(req, res, next) {
	    // issue a remember me cookie if the option was checked
	    
	    var token = new Token();
	    token.tokenId = rndstr.randomString(64);
	    token.userId = req.user.id;
	    token.save(function(err) {
		    if (err) { return done(err); }
		    res.cookie('remember_me', token, { path: '/', httpOnly: false, maxAge: 604800000 }); // 7 days
		    return next();
		})},
	function(req, res) {
	    return res.json("Welcome " + req.user);
	});

app.get('/api/user/auth/logout', Auth.isAuthenticated, function(req, res){
	req.logout();
	res.clearCookie("remember_me");
	return res.json({success: "Logged out"});
    });

app.get("/api/user/profile", Auth.isAuthenticated, function(req, res) {
	res.json(req.user);
    });

app.get('/notfound', function(req, res) {
	return res.status(404).send('Not found');
    });

app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function(){
	console.log('Express server listening on port ' + app.get('port'));
    });
https.createServer(credentials, app).listen(app.get('port_ssl'), function(){
	console.log('Express server listening on port ' + app.get('port_ssl'));
    });
