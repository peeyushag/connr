var lastStore = null;
angular.module('ionicApp', ['ionic', 'ionic.weather.services', 'ionic.weather.filters', 'ionic.weather.directives', 'google-maps'])

.constant('FLICKR_API_KEY', '5fcd19557edf5e4145e22684fc4a0ee6')
    .config(function ($stateProvider, $urlRouterProvider)
    {

        // TODO Need to redirect in case of unauthorized
        $stateProvider
            .state('signin',
            {
                url: "/sign-in",
                templateUrl: "sign-in.html",
                controller: 'SignInCtrl'
            })
            .state('connrmenu',
            {
                url: "/connr",
                abstract: true,
                templateUrl: "connr-menu.html"
            })
            .state('connrmenu.home',
            {
                url: "/home",
                views:
                {
                    'menuContent':
                    {
                        templateUrl: "home.html",
                        controller: "HomeCtrl"
                    }
                },
                resolve:
                {
                    Geo: "Geo",
                    ConnRapp: "ConnRapp",
                    $rootScope: "$rootScope",
                    $ionicLoading: "$ionicLoading",
                    homeinit: function (Geo, ConnRapp, $rootScope, $ionicLoading)
                    {
                        var homeObj = {
                            lat: '',
                            lng: '',
                            stores: [],
                            offers: '',
                        };

                        setTimeout(function ()
                        {
                            $rootScope.loading = $ionicLoading.show(
                            {
                                content: 'Loading...',
                                showBackdrop: false
                            });
                        }, 1);

                        return Geo.getLocation().then(function (position)
                        {
                            homeObj.lat = position.coords.latitude;
                            homeObj.lng = position.coords.longitude;
                            var numStores = 20;
                            return ConnRapp.getStoresNear(homeObj.lng, homeObj.lat, numStores).then(function (responseText)
                            {
                                homeObj.stores = responseText["stores"];
                                return ConnRapp.getSubsribedActiveOffers().then(function (offers)
                                {
                                    console.log("Resolve offers: " + JSON.stringify(offers));
                                    homeObj.offers = offers;
                                    return (homeObj);
                                }, function (error)
                                {
                                    console.log("Error getting subscribed offers");
                                });
                            }, function (error)
                            {
                                console.log("Error retrieving stores");
                            });
                            //_this.getCurrent(lat, lng);
                        }, function (error)
                        {
                            homeObj.lat = 24;
                            homeObj.lng = 84;
                            var numStores = 20;
                            return ConnRapp.getStoresNear(homeObj.lng, homeObj.lat, numStores).then(function (responseText)
                            {
                                homeObj.stores = responseText["stores"];
                                return ConnRapp.getSubsribedActiveOffers().then(function (offers)
                                {
                                    console.log("Resolve offers: " + JSON.stringify(offers));
                                    homeObj.offers = offers;
                                    return (homeObj);
                                }, function (error)
                                {
                                    console.log("Error getting subscribed offers");
                                });
                            }, function (error)
                            {
                                console.log("Error retrieving stores");
                            });
                        });

                    }
                }
            })
            .state('connrmenu.checkin',
            {
                url: "/check-in",
                views:
                {
                    'menuContent':
                    {
                        templateUrl: "check-in.html",
                        controller: "CheckinCtrl"
                    }
                },
                resolve:
                {
                    geo: "Geo",
                    connr: "ConnRapp",
                    $rootScope: "$rootScope",
                    $ionicLoading: "$ionicLoading",
                    store: function (geo, connr, $rootScope, $ionicLoading)
                    {
                        var strObj = {
                            name: '',
                            desc: '',
                            offers: [],
                            email: '',
                            ids: '',
                            Subscribed: false,
                        };

                        setTimeout(function ()
                        {
                            $rootScope.loading = $ionicLoading.show(
                            {
                                content: 'Loading...',
                                showBackdrop: false
                            });
                        }, 1);

                        return geo.getLocation().then(function (position)
                        {
                            console.log(position);
                            var lat = position.coords.latitude;
                            var lng = position.coords.longitude;
                            var numStores = new Number(1);
                            //Find store in db
                            return connr.getStoresNear(lng, lat, numStores).then(function (responseText)
                            {
                                strObj.name = responseText["stores"][0]["name"];
                                strObj.desc = responseText["stores"][0]["desc"];
                                strObj.email = responseText["stores"][0]["email"];
                                strObj.ids = responseText["stores"][0]["_id"];
                                console.log(JSON.stringify(strObj) + "::strObj1");
                                //Get subscribed to store or not
                                return connr.storeSubscribed(strObj.ids).then(function (storeSubscribedData)
                                {
                                    strObj.Subscribed = storeSubscribedData;
                                    console.log(JSON.stringify(strObj) + "::strObj3");
                                    return connr.getStoreActiveOffers(strObj.ids).then(function (offers)
                                    {
                                        strObj.offers = offers;
                                        console.log(JSON.stringify(strObj) + "::strObj2");
                                        return (strObj);
                                    }, function (error)
                                    {
                                        console.log("Are you kidding me1:" + error);
                                    });
                                }, function (error)
                                {
                                    console.log("Are you kidding me2:" + error);
                                });
                            }, function (error)
                            {
                                console.log("Are you kidding me3:" + error);
                            });
                        }, function (error)
                        {
                            console.log("Are you kidding me4:" + error);
                            var lat = 24;
                            var lng = 84;
                            var numStores = new Number(1);
                            //Find store in db
                            return connr.getStoresNear(lng, lat, numStores).then(function (responseText)
                            {

                                strObj.name = responseText["stores"][0]["name"];
                                strObj.desc = responseText["stores"][0]["desc"];
                                strObj.email = responseText["stores"][0]["email"];
                                strObj.ids = responseText["stores"][0]["_id"];
                                console.log(JSON.stringify(strObj) + "::strObj1");
                                //Get subscribed to store or not
                                return connr.storeSubscribed(strObj.ids).then(function (storeSubscribedData)
                                {
                                    strObj.Subscribed = storeSubscribedData;
                                    console.log(JSON.stringify(strObj) + "::strObj3");
                                    return connr.getStoreActiveOffers(strObj.ids).then(function (offers)
                                    {
                                        strObj.offers = offers;
                                        console.log(JSON.stringify(strObj) + "::strObj2");
                                        return (strObj);
                                    }, function (error)
                                    {
                                        console.log("Are you kidding me1:" + error);
                                    });
                                }, function (error)
                                {
                                    console.log("Are you kidding me2:" + error);
                                });
                            }, function (error)
                            {
                                console.log("Are you kidding me3:" + error);
                            });
                        });
                        //return{store: strObj};
                    }
                }
            })
            .state('connrmenu.subscriptions',
            {
                url: "/subscriptions",
                views:
                {
                    'menuContent':
                    {
                        templateUrl: "subscriptions.html",
                        controller: "SubscriptionCtrl"
                    }
                }
            })
            .state('connrmenu.mapview',
            {
                url: "/map-view",
                views:
                {
                    'menuContent':
                    {
                        templateUrl: "map_view.html",
                        controller: "MapCtrl"
                    }
                },
                resolve:
                {
                    Geo: "Geo",
                    ConnRapp: "ConnRapp",
                    $rootScope: "$rootScope",
                    $ionicLoading: "$ionicLoading",
                    mapinit: function (Geo, ConnRapp, $rootScope, $ionicLoading)
                    {
                        var mapObj = {
                            lat: '',
                            lng: '',
                            stores: [],
                            offers: '',
                        };

                        setTimeout(function ()
                        {
                            $rootScope.loading = $ionicLoading.show(
                            {
                                content: 'Loading...',
                                showBackdrop: false
                            });
                        }, 1);

                        return Geo.getLocation().then(function (position)
                        {
                            mapObj.lat = position.coords.latitude;
                            mapObj.lng = position.coords.longitude;
                            var numStores = 20;
                            return ConnRapp.getStoresNear(mapObj.lng, mapObj.lat, numStores).then(function (responseText)
                            {
                                mapObj.stores = responseText["stores"];
                                return ConnRapp.getSubsribedActiveOffers().then(function (offers)
                                {
                                    console.log("Resolve offers: " + JSON.stringify(offers));
                                    mapObj.offers = offers;
                                    return (mapObj);
                                }, function (error)
                                {
                                    console.log("Error getting subscribed offers");
                                });
                            }, function (error)
                            {
                                console.log("Error retrieving stores");
                            });
                            //_this.getCurrent(lat, lng);
                        }, function (error)
                        {
                            console.log("Are you kidding me4:" + error);
                            mapObj.lat = 24;
                            mapObj.lng = 84;
                            var numStores = 20;
                            return ConnRapp.getStoresNear(mapObj.lng, mapObj.lat, numStores).then(function (responseText)
                            {
                                mapObj.stores = responseText["stores"];
                                return ConnRapp.getSubsribedActiveOffers().then(function (offers)
                                {
                                    console.log("Resolve offers: " + JSON.stringify(offers));
                                    mapObj.offers = offers;
                                    return (mapObj);
                                }, function (error)
                                {
                                    console.log("Error getting subscribed offers");
                                });
                            }, function (error)
                            {
                                console.log("Error retrieving stores");
                            });
                        });
                    }
                }

            })
            .state('connrmenu.logout',
            {
                url: "/logout",
                controller: "LogoutCtrl"
            })
	    .state('connrmenu.scan',
	    {
		url: "/scan",
		controller: "ScanCtrl"
	    })

        $urlRouterProvider.otherwise("/sign-in");

    })

.controller('LogoutCtrl', function ($scope, $state, $ionicLoading, ConnRapp)
{
    $scope.connrlogout = function (user)
    {
        $scope.loading = $ionicLoading.show(
        {
            content: 'Signing out',
            showBackdrop: false
        });
        ConnRapp.signOut(user).then(function (success)
        {
            $scope.loading.hide();
            window.localStorage.clear();
            $state.go("signin");
        }, function (error)
        {
	    $scope.loading.hide();
	    alert("Error while signing out");
            console.log(error);
        });
    };
})

.controller('ScanCtrl', function($scope, ConnRapp)
{
    $scope.scan = function (user) {
	cordova.plugins.barcodeScanner.scan(function (result) {
		alert("Tracking item: " + JSON.stringify(result));
		$state.go("home");
	    }, function (error) {
		alert("Scanning failed: " + error);
	    });
    };
})

.controller('MainCtrl', function ($scope, $ionicSideMenuDelegate)
{
    $scope.leftButtons = [
    {
        type: 'button-icon button-clear ion-navicon',
        tap: function (e)
        {
            $ionicSideMenuDelegate.toggleLeft($scope.$$childHead);
        }
    }];

})

.controller('CheckinCtrl', function ($scope, geo, $http, $ionicLoading, connr, store)
{
    if (store || $scope.store)
    {
        $scope.store = {
            name: store.name,
            desc: store.desc,
            offers: [],
            email: store.email,
            ids: store.ids,
            Subscribed: store.Subscribed,
        };
        lastStore = store;
    }
    else if (lastStore)
    {
        $scope.store = {
            name: lastStore.name,
            desc: lastStore.desc,
            offers: [],
            email: lastStore.email,
            ids: lastStore.ids,
            Subscribed: lastStore.Subscribed,
        };
    }
    else
    {
        location.reload();
    };

    $scope.loading.hide();

    $scope.storeSubscriptionChanged = function ()
    {
        connr.toggleSubscription(!$scope.store.Subscribed, $scope.store.ids).then(function (success)
        {
            console.log("Successfully toggled");
        }, function (error)
        {
            console.log("Error toggling");
        });
    };
})
    
.controller('SignInCtrl', function ($scope, $state, $http, $ionicLoading, ConnRapp)
{
    var is_loggedin = window.localStorage.getItem("loggedin");
    if (is_loggedin != null && is_loggedin == "true")
    {
        $state.go('connrmenu.home');
    }

    $scope.signIn = function (user)
    {
	$scope.loading = $ionicLoading.show(
        {
            content: 'Signing in',
            showBackdrop: false
        });

	ConnRapp.signInUser(user).then(function (success)
        {
            $scope.loading.hide();
            window.localStorage.setItem("loggedin", "true");
	    $state.go('connrmenu.home');
        }, function (error)
        {
	    $scope.loading.hide();
	    alert("Error while signing in");
            console.log(error);
        });
    };
})

.controller("HomeCtrl", function ($scope, $timeout, $rootScope, Geo, Flickr, $ionicPlatform, $http, $ionicLoading, ConnRapp, homeinit, $ionicModal)
{
    var _this = this;
    $scope.activeBgImageIndex = 0;
    $scope.numStores = new Number(20);

    if (homeinit)
    {
        $scope.homeObj = {
            lat: homeinit.lat,
            lng: homeinit.lng,
            offers: homeinit.offers,
            stores: homeinit.stores,
        };
        $scope.offers = $scope.homeObj.offers;
        $scope.stores = $scope.homeObj.stores;
        console.log("Current location" + homeinit.lat + homeinit.lng);
    }
    else
    {
        console.log("Home Init is null. Long live home init");
        window.location.href = "#/connr/home";
    };

    $ionicModal.fromTemplateUrl('modal_offer.html', function (modal)
    {
        $scope.offerModal = modal;
    },
    {
        animation: 'slide-in-up',
        focusFirstInput: true,
        scope: $scope
    });

    $scope.getOfferModal = function (offer)
    {
        $scope.activeOffer = offer;
        ConnRapp.registerOfferClick(offer._id, homeinit.lat, homeinit.lng).then(function (success)
        {
            console.log("Registered click");
        }, function (error)
        {
            console.log("Error");
        });
        $scope.offerModal.show();
    }

    $ionicModal.fromTemplateUrl('modal_store.html', function (modal)
    {
        $scope.storeModal = modal;
    },
    {
        animation: 'slide-in-up',
        focusFirstInput: true,
        scope: $scope
    });


    $scope.getStoreModal = function (store)
    {
        $scope.activeStore = store;
        /*ConnRapp.registerOfferClick(offer._id,homeinit.lat,homeinit.lng).then(function(success) {
        console.log("Registered click");
        }, function(error) {
        console.log("Error");
        });*/
        $scope.storeModal.show();
    }

    $scope.getClickedStore = function (store)
    {
        $scope.activeStore = store;
        ConnRapp.registerOfferClick(offer._id, homeinit.lat, homeinit.lng).then(function (success)
        {
            console.log("Registered click");
        }, function (error)
        {
            console.log("Error");
        });
    }

    this.getBackgroundImage = function (lat, lng, locString)
    {
        Flickr.search(locString, lat, lng).then(function (resp)
        {
            var photos = resp.photos;
            if (photos.photo.length)
            {
                $scope.bgImages = photos.photo;
                _this.cycleBgImages();
            }
        }, function (error)
        {
            console.error('Unable to get Flickr images', error);
        });
    };

    this.cycleBgImages = function ()
    {
        $timeout(function cycle()
        {
            if ($scope.bgImages)
            {
                console.log(JSON.stringify($scope.activeBgImage));
                $scope.activeBgImage = $scope.bgImages[$scope.activeBgImageIndex++ % $scope.bgImages.length];
                console.log(JSON.stringify($scope.activeBgImage));
                console.log("Inside cycle images");
            }
        });
    };

    $scope.refreshData = function ()
    {
        if ($scope.homeObj == null)
        {
            $scope.homeObj = {
                lat: 10,
                lng: 10
            };
            Geo.reverseGeocode(10, 10).then(function (locString)
            {
                $scope.loading.hide();
                _this.getBackgroundImage($scope.homeObj.lat, $scope.homeObj.lng, locString);
            });
        }
        else
        {
            Geo.reverseGeocode(10, 10).then(function (locString)
            {
                $scope.loading.hide();
                _this.getBackgroundImage($scope.homeObj.lat, $scope.homeObj.lng, locString);
            });
        }
    };

    $scope.refreshData();
    
    setTimeout(function() {
	    $(".live-tile, .flip-list").not(".exclude").liveTile();
	}, 5000);
})

.controller('SubscriptionCtrl', function ($scope, $ionicLoading, ConnRapp)
{
    $scope.stores = [];
    $scope.loading = $ionicLoading.show(
    {
        content: 'Loading subscriptions',
        showBackdrop: false
    });

    $scope.subscriptionsLoad = function ()
    {
        ConnRapp.getSubscribedStores().then(function (stores)
        {
            console.log("Loaded here subscribed: " + JSON.stringify(stores));
            if (stores != null && stores.length > 0)
            {
                $scope.stores = [];
                for (var i = 0; i < stores.length; i++)
                {
                    $scope.stores.push(
                    {
                        name: stores[i].name,
                        description: stores[i].desc,
                        subscribed: true,
                        id: stores[i]["_id"]
                    });
                }
                $scope.loading.hide();
            }
        });
    }
    $scope.subscriptionsLoad();
    // Called when the store subscription changes
    $scope.subscribedChange = function (store)
    {
        var subscriptionStatus = !store.subscribed;
        console.log(JSON.stringify(store));
        ConnRapp.toggleSubscription(subscriptionStatus, store.id).then(function (success)
        {
            console.log("Toggled");
        }, function (error)
        {
            console.log("Error");
        });
    };
})
.controller('MapCtrl', function ($scope, mapinit)
    {
        if (mapinit)
    {
        $scope.mapObj = {
            lat: mapinit.lat,
            lng: mapinit.lng,
            offers: mapinit.offers,
            stores: mapinit.stores,
        };
        $scope.offers = $scope.mapObj.offers;
        $scope.stores = $scope.mapObj.stores;
        console.log("Current location" + mapinit.lat + mapinit.lng);
         $scope.loading.hide();
    }
    else
    {
        console.log("Home Init is null. Long live home init");
        window.location.href = "#/connr/home";
    };
        $scope.map = {
            center:
            {
                latitude: $scope.mapObj.lat,
                longitude: $scope.mapObj.lng
            },
            zoom: 17,
            draggable: true,
            markers2: [
            {
                latitude: 46,
                longitude: -77,
                showWindow: false,
                title: '[46,-77]'
            },
            {
                latitude: 33,
                longitude: -77,
                showWindow: false,
                title: '[33,-77]'
            },
            {
                latitude: 35,
                longitude: -125,
                showWindow: false,
                title: '[35,-125]'
            }]
        };
    })

.filter("filter2", function ()
{
    return function (input, test)
    {
        var newArray = [];
        for (var x = 0; x < input.length; x += 2)
        {
            newArray.push(input[x]);
        }
        return newArray;
    }
})

.filter("filter3", function ()
{
    return function (input, test)
    {
        var newArray = [];
        for (var x = 0; x < input.length; x += 3)
        {
            newArray.push(input[x]);
        }
        return newArray;
    }
});
