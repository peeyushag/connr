angular.module('ionic.weather.services', ['ngResource'])

    .factory('Geo', function($q, $window, $timeout) {
	var latlong = null;
  return {
    reverseGeocode: function(lat, lng) {
      var q = $q.defer();

      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({
        'latLng': new google.maps.LatLng(lat, lng)
      }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
	    //console.log('Reverse', JSON.stringify(results));
          if(results.length > 0) {
            var r = results[0];
            var a, types;
            var parts = [];
            var foundLocality = false;
            var foundState = false;
            for(var i = 0; i < r.address_components.length; i++) {
              a = r.address_components[i];
              types = a.types;
              for(var j = 0; j < types.length; j++) {
                if(!foundLocality && types[j] == 'locality') {
                  foundLocality = true;
                  parts.push(a.long_name);
                } else if(!foundState && types[j] == 'administrative_area_level_1') {
                  foundState = true;
                  //parts.push(a.long_name);
                }
              }
            }
            console.log('Reverse', parts);
            q.resolve(parts.join(' '));
          }
        } else {
          console.log('reverse fail', results, status);
          q.reject(results);
        }
      })

      return q.promise;
    },
getLocation: function() {
      var q = $q.defer();
      var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
      };
      
      function success(pos) {
	  q.resolve(pos);
      };
      
      function error(err) {
	  q.reject(err);
      };
      
      if($window.navigator && $window.navigator.geolocation){
	 $window.navigator.geolocation.getCurrentPosition(success, error, options);
      }
      
      return q.promise;
    }
  };
})

.factory('Flickr', function($q, $resource, FLICKR_API_KEY) {
  var baseUrl = 'http://api.flickr.com/services/rest/'


  var flickrSearch = $resource(baseUrl, {
    method: 'flickr.groups.pools.getPhotos',
    group_id: '1463451@N25',
    safe_search: 1,
    jsoncallback: 'JSON_CALLBACK',
    api_key: FLICKR_API_KEY,
    format: 'json'
  }, {
    get: {
      method: 'JSONP'
    }
  });

  return {
    search: function(tags, lat, lng) {
      var q = $q.defer();
  
      console.log('Searching flickr for tags', tags);

      flickrSearch.get({
        tags: tags,
        lat: lat,
        lng: lng
      }, function(val) {
        q.resolve(val);
      }, function(httpResponse) {
        q.reject(httpResponse);
      });

      return q.promise;
    }
  };
})

.factory('ConnRapp', function($q, $http) {
  return {
    storeSubscribed: function(store_id) {
      var q = $q.defer();
      $http({method: 'GET',
        url: 'http://connr.us/api/user/profile',
        withCredentials: true
             }).success(function(responseText, status, xhr, $form) {
              //If successful then check if current store in list of subscribed stores and toggle accordingly              
               var storeList = JSON.stringify(responseText.subscribed);
               console.log("Id store: "+ storeList);
               if(storeList.indexOf(store_id) >= 0){
                console.log("Dir Already subscribed");
                var storeSubscribedData = true;
                 q.resolve(storeSubscribedData);
               }
               else{
                console.log("Dir Not subscribed");
                var storeSubscribedData = false;
                 q.resolve(storeSubscribedData);
              }
           }).error(function(data, status, headers, config){
             console.log("Cannot get profile data ");
             var error = "Cannot get profile data";
             q.reject(error);
               });

      return q.promise;
    },
    getStoreActiveOffers: function(store_id) {
      var q = $q.defer();
      $http({method: 'GET',
          url: 'http://connr.us/api/user/get_active_soffers',
          params: {id: store_id},
          withCredentials: true
               }).success(function(responseText, status, xhr, $form) {
                 var offers=responseText["stores"];
                 q.resolve(offers);
             }).error(function(data, status, headers, config){
              var error = "Cannot get store offers";
              q.reject(error);
             });
      return q.promise;
    },
    getSubscribedStores: function() {
	  var q = $q.defer();
	  $http({method: 'GET',
	     url: 'http://connr.us/api/user/get_subscribed_stores',
	     withCredentials: true
		  }).success(function(responseText, status, xhr, $form) {
          console.log(responseText+"::Here");
		      var stores = responseText["stores"];
		      q.resolve(stores);
		 }).error(function(data, status, headers, config) {
		      var error = "Cannot get stores";
		      q.reject(error);
		 });
	  return q.promise;
    },
    getSubsribedActiveOffers: function() {
        var q = $q.defer();
        $http({
          method: 'GET',
          url: 'http://connr.us/api/user/get_all_offers',
          withCredentials: true,
      }).success(function (responseText, status, xhr, $form) {
          var offers = responseText["offers"];
          console.log("Subscribed offers:" + JSON.stringify(responseText));
          q.resolve(offers);
      }).error(function (data, status, headers, config) {
          console.log("Cant get Subscribed offers: " + data);
          var error = "Cannot get store offers";
          q.reject(error);
      });
    return q.promise;
    },
    getStoresNear: function(lng, lat, numStores){
      var q = $q.defer();
      $http({method: 'GET',
           url: 'http://connr.us/api/user/get_stores_near',
           withCredentials: true,
           params: {lon: lng, lat: lat, limit: numStores}
        }).success(function(responseText, status, xhr, $form) {
             q.resolve(responseText);
        }).error(function(data, status, headers, config){
             var error = "Cannot find nearest store";
             q.reject(error);
        });

      return q.promise;
    },
    signInUser: function(user) {
      console.log("Called service to signIn");
      var q = $q.defer();
      
      $http({method: 'GET',
            withCredentials: true,
            url: 'http://connr.us/api/user/auth/local?user=' + user.username + '&pass=' + user.password,
        }).success(function (responseText, status, xhr, $form) {
	    if (status == 404) {
		var error = responseText;
		q.reject(error);
	    } else {
		q.resolve(responseText);
	    }
        }).error(function (data, status, headers, config) {
            var error = data;
            q.reject(data);
        });

      return q.promise;
      },
    signOut: function(user) {
      var q = $q.defer();
      
      $http({method: 'GET',
	    withCredentials: true,
	    url: 'http://connr.us/api/user/auth/logout'
       }).success(function (responseText, status, xhr, $form) {
	       q.resolve(responseText);
       }).error(function (data, status, headers, config) {
	       q.reject(error);
       });

      return q.promise;
      },
    registerOfferClick: function(offer_id, latitude, longitude){
      console.log("Called service to register click"+latitude+longitude);
      var q = $q.defer();
      $http({method: 'GET',
           url: 'http://connr.us/api/user/clicked_offer',
           withCredentials: true,
           params: {id: offer_id, lat: latitude, lng: longitude}
        }).success(function(responseText, status, xhr, $form) {
              console.log("Click registered" + JSON.stringify(responseText));
              q.resolve(responseText);
        }).error(function(data, status, headers, config){
             console.log("Cannot register click");
             q.reject(data);
        });
      return q.promise;
    },
    toggleSubscription: function(storeToggleData, store_id){
      var q = $q.defer();
      if (storeToggleData == true) {
	  $http({
		  method: 'GET',
                    url: 'http://connr.us/api/user/subscribe',
                    params: {
                        storeId: store_id
                    },
                    withCredentials: true
                }).success(function (responseText, status, xhr, $form) {
                    console.log("Successfully subscribed: " + (responseText));
                    q.resolve(responseText);
                }).error(function (data, status, headers, config) {
                    console.log("Cannot subscribe");
                    var error = "Cannot subscribe";
                    q.reject(error);
                });
            } else {
                $http({
                    method: 'GET',
                    url: 'http://connr.us/api/user/unsubscribe',
                    params: {
                        storeId: store_id
                    },
                    withCredentials: true
                }).success(function (responseText, status, xhr, $form) {
                    console.log("Successfully unsubscribed: " + (responseText));
                    q.resolve(responseText);
                }).error(function (data, status, headers, config) {
                    console.log("Cannot unsubscribe");
                    var error = "Cannot unsubscribe";
                    q.reject(error);
                });
            };
      return q.promise;
    },
    
  };
})

