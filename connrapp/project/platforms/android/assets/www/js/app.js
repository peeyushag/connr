angular.module('connr', ['ionic'])
	.controller('ConnrCtrl', function($scope, $ionicModal) {
		$scope.offers = [];

	    $scope.toggleMenu = function() {
		$scope.sideMenuController.toggleLeft();
	    };

	    $ionicModal.fromTemplateUrl('new-task.html', function(modal) {
		    $scope.taskModal = modal;

		$ionicModal.fromTemplateUrl('offer.html', function(modal) {
			$scope.offerModal = modal;

		}, {
			scope: $scope,
			animation: 'slide-in-up'
			    });

		// Called when the form is submitted
		$scope.createOffer = function(offer) {
		    $scope.offers.push({
			    title: offer.title
			});
		    $scope.offerModal.hide();
		    offer.title = "";
		};
		
		// Open our new offer modal
		$scope.newOffer = function() {
		    $scope.offerModal.show();
		};
		
		// Close the new offer modal
		$scope.closeNewOffer = function() {
		    $scope.offerModal.hide();
		};

		$scope.getOffers = function() {
		    $http({
			    method: 'JSONP',
			    url: 'https://localhost:3000/api/user/get_active_offers'
			}).success(function(data, status, headers, config) {
				$scope.offers = data;
			    }).error(function(data, status, headers, config) {
				    console.log(data);
				});
		};
	    });
