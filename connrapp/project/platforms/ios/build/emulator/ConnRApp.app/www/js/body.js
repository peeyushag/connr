var lastStore = null;
angular.module('ionicApp', ['ionic', 'ionic.weather.services', 'ionic.weather.filters', 'ionic.weather.directives'])

.constant('FLICKR_API_KEY', '5fcd19557edf5e4145e22684fc4a0ee6')
    .config(function ($stateProvider, $urlRouterProvider) {

        // TODO Need to redirect in case of unauthorized
        $stateProvider
            .state('signin', {
                url: "/sign-in",
                templateUrl: "sign-in.html",
                controller: 'SignInCtrl'
            })
            .state('connrmenu', {
                url: "/connr",
                abstract: true,
                templateUrl: "connr-menu.html"
            })
            .state('connrmenu.home', {
                url: "/home",
                views: {
                    'menuContent': {
                        templateUrl: "home.html",
                        controller: "HomeCtrl"
                    }
                },
                resolve: {
                    Geo: "Geo",
                    ConnRapp: "ConnRapp",
                    homeinit: function(Geo,ConnRapp){
                        var homeObj= {
                            lat: '',
                            lng: '',
                            stores: [],
                            offers: '',
                        };

                    return Geo.getLocation().then(function (position) {
                        homeObj.lat = position.coords.latitude;
                        homeObj.lng = position.coords.longitude;
                        var numStores = 20;
                        return ConnRapp.getStoresNear(homeObj.lng, homeObj.lat, numStores).then(function(responseText){
                                homeObj.stores = responseText["stores"];
                                return ConnRapp.getSubsribedActiveOffers().then(function(offers){
                                    console.log("Resolve offers: "+ JSON.stringify(offers));
                                    homeObj.offers = offers;
                                    return(homeObj);
                                },function(error){
                                    console.log("Error getting subscribed offers");
                                });
                            },function(error){
                                console.log("Error retrieving stores");
                            });
                           //_this.getCurrent(lat, lng);
                    }, function (error) {
                        console.log("Are you kidding me4:"+error);
                        homeObj.lat = 24;
                        homeObj.lng = 84;
                        var numStores = 20;
                        return ConnRapp.getStoresNear(homeObj.lng, homeObj.lat, numStores).then(function(responseText){
                                homeObj.stores = responseText["stores"];
                                return ConnRapp.getSubsribedActiveOffers().then(function(offers){
                                    console.log("Resolve offers: "+ JSON.stringify(offers));
                                    homeObj.offers = offers;
                                    return(homeObj);
                                },function(error){
                                    console.log("Error getting subscribed offers");
                                });
                            },function(error){
                                console.log("Error retrieving stores");
                            });
                    });

                    }
                }
            })
            .state('connrmenu.checkin', {
                url: "/check-in",
                views: {
                    'menuContent': {
                        templateUrl: "check-in.html",
                        controller: "CheckinCtrl"
    			    }
    		    },
                resolve: {
                    geo: "Geo",
                    connr: "ConnRapp",
                    store: function(geo,connr){
                        var strObj= {
                            name: '',
                            desc: '',
                            offers: [],
                            email: '',
                            ids: '',
                            Subscribed: false,
                        };
                         return geo.getLocation().then(function (position) {
                            console.log(position);
                                var lat = position.coords.latitude;
                                var lng = position.coords.longitude;
                                var numStores = new Number(1);
                                //Find store in db
                                return connr.getStoresNear(lng, lat, numStores).then(function (responseText) {
                                        
                                        strObj.name = responseText["stores"][0]["name"];
                                        strObj.desc = responseText["stores"][0]["desc"];
                                        strObj.email = responseText["stores"][0]["email"];
                                        strObj.ids = responseText["stores"][0]["_id"];
                                        console.log(JSON.stringify(strObj)+"::strObj1");
                                    //Get subscribed to store or not
                                    return connr.storeSubscribed(strObj.ids).then(function (storeSubscribedData) {
                                        strObj.Subscribed = storeSubscribedData;
                                        console.log(JSON.stringify(strObj)+"::strObj3");
                                        return connr.getStoreActiveOffers(strObj.ids).then(function (offers) {
                                            strObj.offers = offers;
                                            console.log(JSON.stringify(strObj)+"::strObj2");
                                            return (strObj);
                                        }, function (error) { 
                                            console.log("Are you kidding me1:"+error);
                                        });
                                    }, function (error) {
                                        console.log("Are you kidding me2:"+error);
                                    });
                                }, function (error) {
                                    console.log("Are you kidding me3:"+error);
                                });
                            },function (error) {
                                    console.log("Are you kidding me4:"+error);
                                    var lat = 24;
                                var lng = 84;
                                var numStores = new Number(1);
                                //Find store in db
                                return connr.getStoresNear(lng, lat, numStores).then(function (responseText) {
                                        
                                        strObj.name = responseText["stores"][0]["name"];
                                        strObj.desc = responseText["stores"][0]["desc"];
                                        strObj.email = responseText["stores"][0]["email"];
                                        strObj.ids = responseText["stores"][0]["_id"];
                                        console.log(JSON.stringify(strObj)+"::strObj1");
                                    //Get subscribed to store or not
                                    return connr.storeSubscribed(strObj.ids).then(function (storeSubscribedData) {
                                        strObj.Subscribed = storeSubscribedData;
                                        console.log(JSON.stringify(strObj)+"::strObj3");
                                        return connr.getStoreActiveOffers(strObj.ids).then(function (offers) {
                                            strObj.offers = offers;
                                            console.log(JSON.stringify(strObj)+"::strObj2");
                                            return (strObj);
                                        }, function (error) { 
                                            console.log("Are you kidding me1:"+error);
                                        });
                                    }, function (error) {
                                        console.log("Are you kidding me2:"+error);
                                    });
                                }, function (error) {
                                    console.log("Are you kidding me3:"+error);
                                });
                        });
                        //return{store: strObj};
                    }
                }
            })

            .state('connrmenu.subscriptions', {
                url: "/subscriptions",
                views: {
                    'menuContent': {
                        templateUrl: "subscriptions.html",
                        controller: "SubscriptionCtrl"
			    }
		    }
		})

	    .state('connrmenu.logout', {
		    url: "/logout",
			controller: "LogoutCtrl"
			})

        $urlRouterProvider.otherwise("/sign-in");

    })

.controller('LogoutCtrl', function ($scope, $state, ConnRapp) {
	$scope.connrlogout = function(user) {
	    ConnRapp.signOut(user).then(function(success) {
		    console.log("Successfully signed out");
		    $state.go("signin");
		}, function(error) {
		    console.log(error);
		});
	};
    })

.controller('MainCtrl', function ($scope, $ionicSideMenuDelegate) {
    $scope.leftButtons = [{
        type: 'button-icon button-clear ion-navicon',
        tap: function (e) {
            $ionicSideMenuDelegate.toggleLeft($scope.$$childHead);
        }
    }];

})

.controller('CheckinCtrl', function ($scope, geo, $http, connr, store) {
    console.log(store);
    if(store || $scope.store){
        $scope.store = {
            name: store.name,
            desc: store.desc,
            offers: [],
            email: store.email,
            ids: store.ids,
            Subscribed: store.Subscribed,
        };
	lastStore = store;
    console.log(JSON.stringify(lastStore));
    } else if (lastStore) {
    console.log(JSON.stringify(lastStore));
	$scope.store = {
            name: lastStore.name,
            desc: lastStore.desc,
            offers: [],
            email: lastStore.email,
            ids: lastStore.ids,
            Subscribed: lastStore.Subscribed,
        };
    } else {
        location.reload();
    };
        
        $scope.storeSubscriptionChanged = function () {
            connr.toggleSubscription(!$scope.store.Subscribed, $scope.store.ids).then(function(success){
                console.log("Successfully toggled");
            },function(error){
                console.log("Error toggling");
            });
        };
    })

.controller('SignInCtrl', function($scope, $state, $http) {
	var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        function success(pos) {
            var crd = pos.coords;

            alert(crd.latitude + " " + crd.longitude);
            alert(crd.accuracy);
        };

        function error(err) {
            alert(err);
        };

        navigator.geolocation.getCurrentPosition(success, error, options);
        $scope.signIn = function(user) {
        console.log("Not in here");
	$http({method: 'GET',
		    withCredentials: true,
		    url: 'http://54.186.206.111/api/user/auth/local?user=' + user.username + '&pass=' + user.password,
            }).success(function(responseText, status, xhr, $form) {
                console.log(status);
                console.log("Success: " + responseText);
                $state.go('connrmenu.home');
            }).error(function(data, status, headers, config){
                console.log("Sign in errror: " + data);
                alert("Incorrect username/password combination");
            });
        };  
    })
    
.controller('ModalCtrl', function($scope) {
  
  $scope.newUser = {}; 
  
  $scope.createContact = function() {
    console.log('Create Contact', $scope.newUser);
    $scope.modal.hide();
  };
  
})


.controller("HomeCtrl", function ($scope, $timeout, $rootScope, Geo, Flickr, $ionicPlatform, $http, ConnRapp, homeinit, $ionicModal) {

    /*var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        function success(pos) {
            var crd = pos.coords;

            alert(crd.latitude + " Home " + crd.longitude);
            alert(crd.accuracy);
        };

        function error(err) {
            alert(err);
        };

        navigator.geolocation.getCurrentPosition(success, error, options);
*/


    var _this = this;
    $scope.activeBgImageIndex = 0;
    $scope.numStores = new Number(20);
    
    //    console.log("HomeInit offers:: "+JSON.stringify(homeinit.offers));
    
    if(homeinit){
        $scope.homeObj = {
            lat: homeinit.lat,
            lng: homeinit.lng,
            offers: homeinit.offers,
            stores: homeinit.stores,
        };
        $scope.offers=$scope.homeObj.offers;
        $scope.stores=$scope.homeObj.stores;
        console.log("Current location"+ homeinit.lat + homeinit.lng);
    } else {
        console.log("Home Init is null. Long live home init");
        window.location.href = "#/connr/home";
    };   
    
    $ionicModal.fromTemplateUrl('modal.html', function(modal) {
        $scope.modal = modal;
    }, {
	animation: 'slide-in-up',
	focusFirstInput: true,
        scope: $scope 
    });
    
    
    $scope.getOfferModal = function(offer){
	$scope.activeOffer = offer;
	ConnRapp.registerOfferClick(offer._id,homeinit.lat,homeinit.lng).then(function(success) {
		console.log("Registered click");
	    }, function(error) {
		console.log("Error");
	    });
	$scope.modal.show();
    }

    this.getBackgroundImage = function (lat, lng, locString) {
        Flickr.search(locString, lat, lng).then(function (resp) {
            var photos = resp.photos;
            if (photos.photo.length) {
                //	    console.log(photos.photo);
                $scope.bgImages = photos.photo;
                _this.cycleBgImages();
            }
        }, function (error) {
            console.error('Unable to get Flickr images', error);
        });
    };

    this.cycleBgImages = function () {
        $timeout(function cycle() {
            if ($scope.bgImages) {
                console.log(JSON.stringify($scope.activeBgImage));
                $scope.activeBgImage = $scope.bgImages[$scope.activeBgImageIndex++ % $scope.bgImages.length];
                console.log(JSON.stringify($scope.activeBgImage));
                console.log("Inside cycle images");
            }
            //$timeout(cycle, 10000);     
        });
    };


    $scope.refreshData = function () {
        if($scope.homeObj == null) {
            //location.reload();
	    console.log("HEYYYOOO");
	  $scope.homeObj = {lat: 10, lng: 10};
          Geo.reverseGeocode(10, 10).then(function (locString) {
		  _this.getBackgroundImage($scope.homeObj.lat, $scope.homeObj.lng, locString);
	      });
	} else {
        console.log
	    Geo.reverseGeocode(10, 10).then(function (locString) {
		  _this.getBackgroundImage($scope.homeObj.lat, $scope.homeObj.lng, locString);
	      });
	}
    };

    $scope.refreshData();



    console.log("I am here!");


    $scope.sponses = [{
        name: "yolo",
        description: "desc"
    }, {
        name: "haha",
        description: "blah"
    }, {
        name: "PS4",
        description: "gimme some love man"
    }, {
        name: "XBOX",
        description: "desc"
    }, {
        name: "Something else",
        description: "blah"
    }, {
        name: "hey yall",
        description: "gimme some love man"
    }];

})

    .controller('SubscriptionCtrl', function ($scope, ConnRapp) {
    $scope.stores = [];

    $scope.subscriptionsLoad = function () {
	ConnRapp.getSubscribedStores().then(function(stores) {
        console.log("Loaded here subscribed: "+JSON.stringify(stores));
		if (stores != null && stores.length > 0) {
		    $scope.stores= [];
		    for (var i = 0; i < stores.length; i++) {
			$scope.stores.push({name: stores[i].name,
				    description: stores[i].desc,
				    subscribed: true,
				    id: stores[i]["_id"]});
		    }
		}
	    });
    }
    $scope.subscriptionsLoad();
    // Called when the store subscription changes
    $scope.subscribedChange = function (store) {
	var subscriptionStatus = !store.subscribed;
	console.log(JSON.stringify(store));
	ConnRapp.toggleSubscription(subscriptionStatus, store.id).then(function(success) {
		console.log("Toggled");
	    }, function(error) {
		console.log("Error");
	    });
    };
})

.filter("filter3", function () {
    return function (input, test) {
        var newArray = [];
        for (var x = 0; x < input.length; x += 3) {
            newArray.push(input[x]);
        }
        return newArray;
    }
});