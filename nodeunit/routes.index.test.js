var indexRoutes = require('../routes/index.js');

exports.testAddOffer = function(test) {
    /*  var offers = [
{ start : new Date().toString(),
  investment : "$100",
  name : 'Learn AngularJS'
}
	       ];
  var fn = indexRoutes.addOffer(offers);
    */
  var d = new Date();
  var req = {
      body : {
	  start : d.toString(),
	  investment : "$0",
	  name : 'Learn TDD'
      }
  };

  var Offer = function(obj) {
      this.data = obj;
      this.save = function(callback) {
	  test.equals(obj, req.body);
	  callback(null, this);
      };
  };

  var fn = indexRoutes.addOffer(Offer);


  var res = {
      json : function(obj) {

	  /*	  test.equals(offers, obj.offers);
	  test.equals(2, offers.length);
	  test.equals(req.body.name, offers[1].name);
	  test.equals(d.toString(), offers[1].start);
	  //test.ok(!offers[1].done);
	  // test.expect means 'expect 5 tests will be run'
	  test.expect(4);
	  */
	  test.equals(req.body, obj.todo.data);
	  test.expect(2);
	  test.done();
      }
  };

  fn(req, res);
};