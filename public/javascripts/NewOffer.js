var app = angular.module('NewOffer', ['ui.bootstrap','ngRoute','angularFileUpload']);

var DatepickerDemoCtrl = function ($scope) {
    $scope.today = function() {
	$scope.dt = new Date();
    };
    $scope.today();

    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
	$scope.showWeeks = ! $scope.showWeeks;
    };

    $scope.clear = function () {
	$scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
	return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
	$scope.minDate = ( $scope.minDate ) ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
	$event.preventDefault();
	$event.stopPropagation();
	$scope.opened = true;
    };

    $scope.dateOptions = {
	'year-format': "'yy'",
	'starting-day': 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
};

app.config(function($routeProvider, $locationProvider) {
	$routeProvider.when('/showExisting', {
		templateUrl: '/html/showExisting.html',
		    controller: showExistCntl,
		    });
	$routeProvider.when('/createNew', {
		templateUrl: '/html/createNew.html',
		    controller: createNewCntl
		    });
	$routeProvider.when('/mainDashboard', {
		templateUrl: '/html/mainDashboard.html',
		    controller: dashboardCntl
		    });
	$routeProvider.when('/controlPanel', {
		templateUrl: '/html/controlPanel.html',
		    controller: controlPanelCntl
		    });
	$routeProvider.otherwise({
		redirectTo: '/mainDashboard'
		    });
	// configure html5 to get links working on jsfiddle
	$locationProvider.html5Mode(true);
    });

function controlPanelCntl($scope, $routeParams) {
    $scope.name = "controlPanelCntl";
    $scope.params = $routeParams;

    //google.maps.event.addDomListener(window, "load", function() {
    //
    // initialize map
    //
    var map = new google.maps.Map(document.getElementById("mapdiv"), {
      center: new google.maps.LatLng(22.57265, 88.36389),
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    //
    // initialize marker
    //
    var marker = new google.maps.Marker({
      position: map.getCenter(),
      draggable: true,
      map: map
    });
    //
    // intercept map and marker movements
    //
    google.maps.event.addListener(map, "idle", function() {
      marker.setPosition(map.getCenter());
      document.getElementById("mapoutput1").value = map.getCenter().lat().toFixed(6) + ","+ map.getCenter().lng().toFixed(6);
    });
    google.maps.event.addListener(marker, "dragend", function(mapEvent) {
      map.panTo(mapEvent.latLng);
    });
    //
    // initialize geocoder
    //
    var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(document.getElementById("Find"), 'click', function(domEvent) {
      if (domEvent.preventDefault){
        domEvent.preventDefault();
      } else {
        domEvent.returnValue = false;
      }
      geocoder.geocode({
        address: document.getElementById("mapinput").value
      }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          var result = results[0];
          document.getElementById("mapinput").value = result.formatted_address;
          if (result.geometry.viewport) {
            map.fitBounds(result.geometry.viewport);
          }
          else {
            map.setCenter(result.geometry.location);
          }
        } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
          alert("Sorry, the geocoder failed to locate the specified address.");
        } else {
          alert("Sorry, the geocoder failed with an internal error.");
        }
      });
    });
  //}); 

    $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
		var $total = navigation.find('li').length;
		var $current = index+1;
		var $percent = ($current/$total) * 100;
		
	    }});


 $('#moderators').dataTable( {
      "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
    "bProcessing": true,
    //"sAjaxSource": "/api/user/dTableOffers",
    "sAjaxSource": "/api/admin/show_sub_admins",
    "sWrapper": "dataTables_wrapper form-inline",
    "aoColumns": [
          { "sTitle": "Admin Name", "sClass": "center","mDataProp": "name" },
          { "sTitle": "Mobile" , "sClass": "center","mDataProp": "mobile"},
          { "sTitle": "email" , "sClass": "center","mDataProp": "email"},
          { "sTitle": "Stores", "sClass": "center", "mDataProp": "stores"  },
          { "sTitle": "Permissions", "sClass": "center",  "mDataProp": "permissions" },
            ]
    } );

 $('#stores').dataTable( {
      "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
    "bProcessing": true,
    //"sAjaxSource": "/api/user/dTableOffers",
    "sAjaxSource": "/api/admin/show_stores",
    "sWrapper": "dataTables_wrapper form-inline",
    "aoColumns": [
          { "sTitle": "Store", "sClass": "center","mDataProp": "name" },
          { "sTitle": "Phone" , "sClass": "center","mDataProp": "phone"},
          { "sTitle": "email" , "sClass": "center","mDataProp": "email"},
          { "sTitle": "Location", "sClass": "center", "mDataProp": "loc"  },
          { "sTitle": "Offers", "sClass": "center",  "mDataProp": "offers" },
            ]
    } );

}

function showExistCntl($scope, $routeParams) {
    $scope.name = "showExistCntl";
    $scope.params = $routeParams;

    $('#offers').dataTable( {
	    "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
		"bProcessing": true,
    //"sAjaxSource": "/api/user/dTableOffers",
		"sAjaxSource": "/api/admin/store/get_offers",
		"sWrapper": "dataTables_wrapper form-inline",
		"aoColumns": [
			    { "sTitle": "Name", "sClass": "center","mDataProp": "name" },
          { "sTitle": "Description" , "sClass": "center","mDataProp": "description"},
          { "sTitle": "Store" , "sClass": "center","mDataProp": "stores"},
          { "sTitle": "Start Date", "sClass": "center", "mDataProp": "start"  },
          { "sTitle": "End Date", "sClass": "center",  "mDataProp": "end" },
			      ]
		} );
}

function createNewCntl($scope, $routeParams) {
    $scope.name = "createNewCntl";
    $scope.params = $routeParams;

    $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
		var $total = navigation.find('li').length;
		var $current = index+1;
		var $percent = ($current/$total) * 100;
		$('.progress-bar').attr({
			"aria-valuenow" : "$percent",
			    "style" : "width:$percent+'%'"
			    });
    }});
}

function dashboardCntl($scope, $routeParams) {
    $scope.name = "dashboardCntl";
    $scope.params = $routeParams;
}
