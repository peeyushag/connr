google.maps.event.addDomListener(window, "load", function() {
    //
    // initialize map
    //
    var map = new google.maps.Map(document.getElementById("mapdiv"), {
      center: new google.maps.LatLng(22.57265, 88.36389),
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    //
    // initialize marker
    //
    var marker = new google.maps.Marker({
      position: map.getCenter(),
      draggable: true,
      map: map
    });
    //
    // intercept map and marker movements
    //
    google.maps.event.addListener(map, "idle", function() {
      marker.setPosition(map.getCenter());
      document.getElementById("mapoutput1").value = map.getCenter().lat().toFixed(6) + ","+ map.getCenter().lng().toFixed(6);
    });
    google.maps.event.addListener(marker, "dragend", function(mapEvent) {
      map.panTo(mapEvent.latLng);
    });
    //
    // initialize geocoder
    //
    var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(document.getElementById("Find"), 'click', function(domEvent) {
      if (domEvent.preventDefault){
        domEvent.preventDefault();
      } else {
        domEvent.returnValue = false;
      }
      geocoder.geocode({
        address: document.getElementById("mapinput").value
      }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          var result = results[0];
          document.getElementById("mapinput").value = result.formatted_address;
          if (result.geometry.viewport) {
            map.fitBounds(result.geometry.viewport);
          }
          else {
            map.setCenter(result.geometry.location);
          }
        } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
          alert("Sorry, the geocoder failed to locate the specified address.");
        } else {
          alert("Sorry, the geocoder failed with an internal error.");
        }
      });
    });
  }); 