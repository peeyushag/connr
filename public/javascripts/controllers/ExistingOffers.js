angular.module('App', ['angularFileUpload']);

function ExistingOffers($scope, $http, $timeout, $upload) {

    $scope.offers = []; 
    $scope.newOffer = {
	start : new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
	name : '',
	investment : '$15'
    };

    $scope.newStore = {
    }


    // When landing on page get all offers to display
    $http.get('/api/user/offers')
	.success(function(data) {
		$scope.offers = data;
		console.log(data);
	    })
	.error(function(data) {
		console.log('Error: ' + data);
	    });
    // To toggle the active area
    $scope.selected = 1;

    $scope.select= function(item) {
	$scope.selected = item; 
    };

    $scope.itemClass = function(item) {
        return item === $scope.selected ? 'active' : undefined;
    };
    

    $scope.storeList = [];
$scope.selectAction = function() {
        console.log($scope.newOffer.stores);
    };

    $http({
            method: 'GET',
            url: '/api/admin/show_stores'
        }).success(function (result) {
        $scope.storeList = result["aaData"];
        console.log("Test accounts: "+JSON.stringify($scope.storeList));
    });
	$scope.file = [];

    $scope.onFileSelect = function($files) {
    	console.log(JSON.stringify($files[0].name) + ":Files name from the end of shore");
    //$files: an array of files selected, each file has name, size, and type.
      $scope.file  = $files;
      $scope.newOffer.image = $files[0].name;
     }

    //To add new offers
    $scope.addNewOffer = function() {
	console.log($scope.newOffer);
	//$scope.progress[index] = 0;
	$scope.upload = $upload.upload({
		url : 'upload',
		method: 'POST',
		headers: {'my-header': 'my-header-value'},
		file: $scope.file,
		fileFormDataName: 'file'
	}).then(function(response) {
		console.log(response);
		//$scope.uploadResult.push(response.data);
	}, null, function(evt) {
		//$scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
	}).xhr(function(xhr){
		//xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
	});

	$http.post('/api/admin/store/add_offer', $scope.newOffer).success(function(data) {
		if (data.offer) {
		    //$scope.offers.push(data.offer);
		    $scope.newOffer.description = '';
		    $scope.newOffer.name = '';
		    $scope.newOffer.stores = '';
		    $scope.newOffer.start = '';
		    $scope.newOffer.end = '';
		    $scope.newOffer.model = '';
		    $scope.newOffer.brands = '';
		    $scope.newOffer.product_type = '';
		    $scope.newOffer.offer_type = '';
		    $scope.newOffer.image = '';
		    // TODO Show a modal here and redirect to dashboard
		} else {
		    alert(JSON.stringify(data));
		}
	    });
    };



    $scope.newStore = {};
    $scope.addNewStore = function() {
	$scope.newStore.latlong = $('#mapoutput1').val();
	$http.post('/api/admin/add_store', $scope.newStore).success(function(data) {
		if (data.store) {
		    //$scope.stores.push(data.store);
		    console.log(data);
		} else {
		    alert(JSON.stringify(data));
		}
		});
    };

    $scope.newMod = {};
    $scope.newMod.value1='',
    $scope.newMod.value2='',
    $scope.newMod.value3='',

    $scope.addNewModerator = function() {
	console.log($scope.newMod.value1+$scope.newMod.value2+$scope.newMod.value3);
	$scope.newMod.permissions = $scope.newMod.value1+$scope.newMod.value2+$scope.newMod.value3;
	$http.post('/api/admin/add_sub_admin', $scope.newMod).success(function(data) {
		if (data.store) {
		    //$scope.mods.push(data.mods); why were you doing this?
		    console.log(data);
		} else {
		    alert(JSON.stringify(data));
		}
	    });
    };

}
