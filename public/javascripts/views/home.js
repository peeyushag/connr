
$(document).ready(function(){
	var hc = new HomeController();
	var av = new AccountValidator();
	
	$("#checkout-form").validate();

	$('#account-form').submit(function() {
		var url = '/api/admin/update_account';
		$.ajax({
			type: "post",
			url: url,
			data: $('#account-form').serialize(),
			beforeSubmit : function(formData, jqForm, options){
			    if (av.validateForm() == false) {
				return false;
			    }
			},
			success	: function(responseText, status, xhr, $form){
			    if (status == 'success') hc.onUpdateSuccess();
			},
			error : function(e){
			    if (e.responseText == 'email-taken'){
				av.showInvalidEmail();
			    } else if (e.responseText == 'username-taken'){
				av.showInvalidUserName();
			    }
			}
		    });
	    });
	
	$('#name-tf').focus();
	$('#github-banner').css('top', '41px');

	// customize the account settings form //
	
	$('#user-tf').attr('disabled', 'disabled');
	$('#account-form-submit').html('Update');

	// setup the confirm window that displays when the user chooses to delete their account //

	$('.modal-confirm').modal({ show : false, keyboard : true, backdrop : true });
	$('.modal-confirm .modal-header h3').text('Delete Account');
	$('.modal-confirm .modal-body p').html('Are you sure you want to delete your account?');
	$('.modal-confirm .cancel').html('Cancel');
	$('.modal-confirm .submit').html('Delete');
	$('.modal-confirm .submit').addClass('btn-danger');

})
    