app.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider
    .state('showExisting', {
        url:'/showExisting',
		    templateUrl: '/html/showExisting.html',
		    controller: showExistCntl,
		    });
	$stateProvider
    .state('createNew', {
        url:'/createNew',
		    templateUrl: '/html/createNew.html',
		    controller: createNewCntl
		    });
	$stateProvider
    .state('mainDashboard', {
      url:'/DashBoard',
		  templateUrl: '/html/mainDashboard.html',
		    controller: dashboardCntl
		    });
	$stateProvider
    .state('controlPanel', {
      url:'/controlPanel',
		  templateUrl: '/html/controlPanel.html',
		    controller: controlPanelCntl
		    });
  $urlRouterProvider.otherwise('/mainDashboard');
  // or
  $urlRouterProvider.otherwise(
    function($injector, $location) {
      $location.path('/mainDashboard');
    });
	// configure html5 to get links working on jsfiddle
	//$urlRouterProvider.otherwise
    //.html5Mode(true);

});