var crypto = require('crypto');
var AM = require('./modules/account-manager');
var EM = require('./modules/email-dispatcher');
var CT = require('./modules/country-list');
var moment = require('moment');


/*
 * GET home page.
 */

// Offers API 
// View home page
exports.index = function(Offer) {
    return function(req, res) {
	Offer.find({}, function(error, offers) {
		return res.render('index', {
			title: 'Connr index',
			offers : offers,
			udata : req.user
		    });
	    });
    };
};
/*
exports.landing = function(){
    return function(req, res) {
	res.render('landingPage', {
		title: 'ConnR - Welcome',
	    });
    };    
};
*/
exports.settings = function(Admin){
    return function(req, res) {
	res.render('home', {
		title : 'Control Panel',
		countries : CT,
		udata : req.user
	    });
    }
};


//Add new offer
exports.addOffer = function(Offer) {
    return function(req, res) {
	var offer = new Offer(req.body);
	offer.save(function(error, offer) {
		if (error || !offer) {
		    res.json({ error : error });
		} else {
		    res.json({ offer : offer });
		}
	    });
    };
};

//Deactivate old Offers
exports.deleteOffer = function(Offer) {
    return function(req, res) {
	/* TODO Validation */
	return Offer.findOneAndUpdate({"_id": req.body.id}, {
		is_active: false, deleted_at: new Date() },
	    function(error, offer) {
		if (error || !offer) {
		    return res.json({ error : error });
		} else {
		    return res.json({ offer : offer });
		}
	    });
    }
};

//Modify an offer, TODO actually modify the offer
exports.modifyOffer = function(Offer) {
    return function(req, res) {
	return Offer.findOneAndUpdate({"_id": req.body.id}, {
		description: req.body.description,
		start: req.body.start,
		end: req.body.end
	    },
	    function(error, offer) {
		if (error || !offer) {
		    return res.json({ error : error });
		} else {
		    return res.json({ offer : offer });
		}
	    });
    };
};


exports.dataTableOffer = function(Offer) {
    return function(req, res) {
	
Offer.find(function(err, offers) {
// if there is an error retrieving, send the error. nothing after res.send(err) will execute
		if (err)
		    res.send(err)

			//res.json(offers); // return all todos in JSON format
			res.send({"aaData":offers});
  });
    };
};


exports.initOffer = function(Offer) {
    return function(req, res) {
        Offer.find(function(err, offers) {
		// if there is an error retrieving, send the error. nothing after res.send(err) will execute                    
		if (err)
		    res.send(err)

			res.json(offers); // return all todos in JSON format
		//res.send({"aaData":offers});
	    });
    };
};


//User stuff

//Store Commands
exports.storeSubscribe = function(Store) {
    return function(req, res) {
	var storeId = req.query.storeId;
	
	Store.find({_id: req.query.storeId}, function(error, stores){
		    if(stores!=null){
			if (req.user.subscribed.indexOf(storeId) >= 0) {
			    return res.json({success: "Successfully subscribed to store"});
			}
			req.user.subscribed.push(storeId);
			req.user.save(function(err, user) { 
				if (err || !user) {  
				    return res.json({error : "Database error"});
				} else { 
				    return res.json({success : "Successfully subscribed to store"});  
				}  
			    });      
		    } else
			return res.json({error:error});
	    });
		/*
	if (storeId == "null" || storeId == null) {
	    return res.status(402).json({error: "Non existent or null store id"});
	}
	if (req.user.subscribed.indexOf(storeId) >= 0) {
	    return res.json({success: "Successfully subscribed to store"});
	}
		
	req.user.subscribed.push(storeId);
	req.user.save(function(err, user) {
		if (err || !user) {
		    return res.json({error : "Database error"});
		} else {
		    return res.json({success : "Successfully subscribed to store"});
		}
	    });
		*/
    };
};

exports.storeUnsubscribe = function(Store) {
    return function(req, res) {
	var storeId = req.query.storeId;
	
	var i = req.user.subscribed.indexOf(storeId);
	
	if(i != -1) {
	    req.user.subscribed.splice(i, 1);
	}
	
	req.user.save(function(err, user) {
		if (err || !user) {
		    return res.json({error : "Database error"});
		} else {
		    
		    return res.json({success : "Successfully unsubscribed from store"});
		}
	    });
    };
};

exports.viewOffer = function(Offer) {
    return function(req, res) {
	var offerId = req.body.offerId;
	Offer.find({_id: req.params.id}, function(error, offer) {
		if (!error && offer) {
		    offer.num_viewed.push(req.user.id);
		    offer.save(function(error, offer) {
			    if (!error && offer) {
				return res.json({success: "Saved view"});
			    } else {
				return res.json({error: err});
			    }
			}); 
		} else {
			return res.json({error: err});
		}
	    });
    };
};

exports.storeGetInfo = function(Store) {
    return function(req, res) {
	return Store.find({_id: req.query.id}, function(error, stores) {
		if (stores != null) {
		    return res.json({stores: stores});
		} else
		    return res.json({error: err});
	    });
    };
};

exports.storeGetSubscribedStores = function(Store) {
    return function(req, res) {
	var subscribedStores = req.user["subscribed"];
	return Store.find({"_id": {$in : subscribedStores}}, function(error, stores) {
		if (stores != null) {
		    return res.json({stores: stores});
		} else {
		    return res.json({error: err});
		}
	    });
    };
};

exports.storeGetAllOffers = function(Offer) {
    return function(req, res) {
	return Offer.find({stores: req.query.id}, function(error, offers) {
                if (offers != null) {
		    return res.json({offers : offers});
		} else
		    return res.json({error: err});
            });
    };
};

// TODO Extremely inefficient implementation. Make better do.
exports.storeGetActiveOffers = function(Offer, Store) {
    return function(req, res) {
	var active_offers = [];
	return Store.findOne({"_id": req.query.id}, function(error, store) {
		Offer.find({}, function(error, offers) {
			for (var i = 0; i < offers.length; i++) {
			    if (offers[i].stores.indexOf(req.query.id) >= 0) {
				active_offers.push(offers[i]);
			    }
			}
			return res.json({offers: active_offers});
		    });
	    });
    };
};

exports.getAllOffers = function(Offer) {
    return function(req, res) {
	return Offer.find({stores: { $in: req.user.subscribed }}, function(error, offers) {
		if (error || !offers) {
		    return res.json({error : "No offers found"});
		} else {
		    return res.json({offers : offers});
		}
	    });
    }
};

exports.getUserClick = function(Offer, ClickObject) {
    return function(req, res) {
	return Offer.findOne({"_id": req.query.id}, function(error, offer) {
		if (error || !offer) {
		    return res.json({error : "No offers found"});
		} else {
		    // Create a click object
		    var lat = req.query.lat;
		    var lng = req.query.lng;
		    var time = new Date();
		    var user_id = req.user.id;

		    var co = new ClickObject();
		    co.user_id = user_id;
		    co.geo = [parseFloat(lng), parseFloat(lat)];
		    co.time = time;
		    co.save(function(error, co) {
			    if (error || !co) {
				console.log(error);
				return res.json({error : error});
			    } else {
				offer.click_objects.push(co._id);
				offer.save(function(error, offer) {
					if (error || !offer) {
					    console.log(error);
					    return res.json({error : error});
					} else {
					    console.log("Click registered");
					    return res.json({success : "Click registered"});
					}
				    });
			    }
			});
		}
	    });
    }
};


exports.getAllActiveOffers = function(Offer) {
    return function(req, res) {
	return Offer.find({is_active: true,
			   stores: { $in: req.user.subscribed }}, function(error, offers) {
		if (error || !offers) {
		    return res.json({error : "No offers found"});
		} else {
		    return res.json({offers : offers});
		}
	    });
    }
};

exports.postUserClick = function(Offer) {
    return function(req, res) {
	return Offer.findOne({"_id": req.body.id}, function(error, offer) {
		if (error || !offer) {
		    return res.json({error : "No offers found"});
		} else {
		    if (offer.num_viewed.indexOf(req.user.id) < 0) {
			offer.num_recipients.push(req.user.id)
		    }
		    offer.save(function(err, offer) {
			    if (err || !offer) {
				console.log("KPI not saved!");
			    } else {
				console.log("Click registered!");
			    }
			    return res.json({success: "Done"});
			});
		}
	    });
    }
};

exports.storeGetNear = function(Store) {
    return function(req, res) {
	Store.find({ loc : {$near : [parseFloat(req.query.lon), parseFloat(req.query.lat)]}})
	.limit(req.query.limit != null ? req.query.limit : 1)
	.exec(function(err, stores) {
		if (err || !stores) {
		    return res.json({ error : err });
		} else {
		    return res.json({ stores : stores });
		}
	    });;
    };
};

// TODO
exports.storeGetWithin = function(Store) {
    return function(req, res) {
	Store.find({ loc : {$near : [parseFloat(req.query.lon), parseFloat(req.query.lat)]}})
	.limit(req.query.limit != null ? req.query.limit : 1)
	.exec(function(err, stores) {
		if (err || !stores) {
		    return res.json({ error : err });
		} else {
		    return res.json({ stores : stores });
		}
	    });;
    };
};


/* Store CRUD operations impl */
//TODO This is complicated. If a subadmin can create a store, that store should
// percolate upwards to all its parents.
exports.addStore = function(Admin, Store) {
    return function(req, res) {
	var store = new Store();
	store.name = req.body.name;
	store.desc = req.body.description;
	store.loc = [parseFloat(req.body.latlong.split(',')[0].trim()),
		     parseFloat(req.body.latlong.split(',')[1].trim())];
	store.address.street1 = req.body.street1;
	store.address.street2 = req.body.street2;
	store.address.city = req.body.city;
	store.address.state = req.body.state;
	store.address.country = req.body.country;
	store.address.zip = req.body.zip;
	store.phone = req.body.phone;
	store.email = req.body.email;
	store.store_id = req.body.store_id;

	store.save(function(error, store) {
		console.log(store);
		if (error || !store) {
		    res.json({ error : error });
		} else {
		    console.log(req.user.id);
		    Admin.findOne({"_id" : req.user.id}, function (err, admin) {
			    if (err || !admin) {
				return res.json({ error : err });
			    } else {
				admin.stores.push(store.id);
				admin.save(function (err, admin) {
					if (err || !admin) {
					    return res.json({ error : err });
					} else {
					    return res.json({ success : "Created new store" });
					}
				    });
			    }
			});
		}
	    });
    };
};

exports.showStores = function(Store) {
    return function(req, res) {
	return Store.find({"_id" : { $in : req.user.stores }, is_active : true},
			  //console.log(stores);
			  function(err, stores) {
			      console.log(stores);
			      console.log(req.user.stores);
			      if (err || !stores) {
				  return res.json({ error : err });
			      } else {
				  res.send({ "aaData" : stores });
			      }
			  });
    };
};

exports.showStore = function(Store) {
    return function(req, res) {
	return Store.find({"_id" : req.query.id}, function(err, store) {
		if (err || !stores) {
		    return res.json({ error : error });
		} else {
		    
		    return res.json({ stores : store });
		}
	    });
    };
};

exports.updateStore = function(Store) {
    return function(req, res) {
	return Store.findOneAndUpdate({ store_id : req.query.store_id }, {
		name: req.body.name,
		desc: req.body.desc,
		loc: [parseFloat(req.body.lon), parseFloat(req.body.lat)],
		address: req.body.address
	    }, function (err, store) {
		if (error || !store) {
		    return res.json({ error : error });
		} else {
		    return res.json({ success : store });
		}
	    });
    };
};

//TODO Check if already deleted_at
exports.deleteStore = function(Store) {
    return function(req, res) {
	return Store.findOneAndUpdate({ store_id : req.query.store_id }, {
		is_active: false,
		deleted_at: !null },
	    function(err, store) {
		if (error || !store) {
		    return res.json({ error : error });
		} else {
		    store.deleted_at = Date.now();
		    store.save(function(err, store) {
			    if (err || !store) {
				return res.json({ error : err });
			    } else {
				return res.json({ success : store });
			    }
			});
		}
	    });
    };
};

/* Admin CRUD ops impl */
exports.addSubAdmin = function(Admin) {
    return function(req, res) {
	Admin.find({"auth.user": req.body.user}, function(err, admin) {
		if (err || !admin) {
		    res.json({error: err});
		} else {
		    saltAndHash(req.body.pass, function(hash){
			    console.log(req.body.permissions);
			    console.log(req.body.user);
			    console.log(req.body.value1);
			    var admin = new Admin();
			    admin.auth.user = req.body.user;
			    admin.auth.pass = hash;
			    admin.email = req.body.email;
			    admin.name=req.body.name;
			    admin.post=req.body.post;
			    admin.mobile=req.body.mobile;
			    admin.permissions = "A"+req.body.value1+req.body.value2+req.body.value3;
			    admin.stores.push(req.body.stores);
			    admin.subadmins = [];
			    // append date stamp when record was created //       
			    admin.create_at = moment().format('MMMM Do YYYY, h:mm:ss a');
			    admin.save(function(error, store) {
				    if (error || !admin) {
					res.json({ error : error + "Failed at 1" });
				    } else {
					Admin.findOne({"_id" : req.user.id}, function(err, padmin) {
						padmin.subadmins.push(admin.id);
						padmin.save(function(error, ppadmin) {
							if (error || !ppadmin) {
							    return res.json({ error : error + "Failed at 2" });
							} else {
							    return res.json({ success : "Successful"});
							}
						    });
					    });
				    }
				});
			});
		}
	    });
    }
};

exports.showSubAdmin = function(Admin) {
    return function(req, res) {
	return Admin.find({"_id" : req.query.admin_id}, function(err, admin) {
		if (err || !admin) {
		    return res.json({ error : error });
		} else {
		    return res.json({ admin : admin });
		}
	    });
    }
};

exports.showSubAdmins = function(Admin) {
    return function(req, res) {
	return Admin.find({"_id" : { $in : req.user.subadmins }},
			  function(err, admins) {
			      if (err || !admins) {
				  return res.json({ error : err });
			      } else {
				  res.send({ "aaData" : admins });
			      }
			  });
    }
};

exports.updateSubAdmin = function(Admin) {
    return function(req, res) {
	return Admin.find({"_id" : req.body.id}, function(err, admin) {
		if (err || !admin) {
		    return res.json({ error : error });
		} else {
		    admin.name = req.body.name;
		    admin.post = req.body.post;
		    admin.mobile = req.body.mobile;
		    admin.email = req.body.email;
		    admin.stores = req.body.stores;
		    admin.permissions = req.body.permissions;
		    admin.save(function(err, admin) {
			    if (err || !admin) {
				return res.json({ error : error });
			    } else {
				return res.json({ admin : admin });
			    }
			});
		}
	    });
    }
};

// TODO Make the delete command have some meaning
exports.deleteSubAdmin = function(Admin) {
    return function(req, res) {
	return Admin.find({"_id" : req.body.id}, function(err, admin) {
		if (err || !admin) {
		    return res.json({ error : error });
		} else {
		    admin.deleted_at = Date.now();
		    admin.save(function(err, admin) {
			    if (err || !admin) {
				return res.json({ error : error });
			    } else {
				return res.json({ admin : admin });
			    }
			});
		}
	    });
    }
};

exports.addAdminPermission = function(Admin) {
    return function(req, res) {
	return Admin.find({"_id" : req.body.id}, function(err, admin) {
		if (err || !admin) {
		    return res.json({ error : error });
		} else {
		    if (admin.permissions.indexOf(req.body.permission) < 0) {
			admin.permissions.push(req.body.permission);
		    }
		    admin.save(function(err, admin) {
			    if (err || !admin) {
				return res.json({ error : error });
			    } else {
				return res.json({ admin : admin });
			    }
			});
		}
	    });
    }
};

exports.removeAdminPermission = function(Admin) {
    return function(req, res) {
	return Admin.find({"_id" : req.body.id}, function(err, admin) {
		if (err || !admin) {
		    return res.json({ error : error });
		} else {
		    var index = admin.permissions.indexOf(req.body.permission); 
		    if (index > 0) {
			admin.permissions.splice(index, 1);
		    }
		    admin.save(function(err, admin) {
			    if (err || !admin) {
				return res.json({ error : error });
			    } else {
				return res.json({ admin : admin });
			    }
			});
		}
	    });
    }
};

exports.addAdminStore = function(Admin) {
    return function(req, res) {
	return Admin.findOne({"_id" : req.body.id}, function(err, admin) {
		if (err || !admin) {
		    return res.json({ error : error });
		} else {
		    if (admin.stores.indexOf(req.body.store_id) < 0) {
			admin.stores.push(req.body.store_id);
		    }
		    admin.save(function(err, admin) {
			    if (err || !admin) {
				return res.json({ error : error });
			    } else {
				return res.json({ admin : admin });
			    }
			});
		}
	    });
    }
};

exports.removeAdminStore = function(Admin) {
    return function(req, res) {
	return Admin.findOne({"_id" : req.body.id}, function(err, admin) {
		if (err || !admin) {
		    return res.json({ error : error });
		} else {
		    var index = admin.stores.indexOf(req.body.store_id); 
		    if (index > 0) {
			admin.stores.splice(index, 1);
		    }
		    admin.save(function(err, admin) {
			    if (err || !admin) {
				return res.json({ error : error });
			    } else {
				return res.json({ admin : admin });
			    }
			});
		}
	    });
    }
};

/* Offers crud operations*/
exports.adminAddOffer = function(Offer, Store) {
    return function(req, res) {
	var offer = new Offer();
	offer.name = req.body.name;
	offer.stores = req.body.stores.split(',');
	offer.offer_type = req.body.offer_type;
	offer.description = req.body.description;
	offer.product_type =req.body.product_type;
	offer.image = req.body.image;
	offer.brands = req.body.brands;
	offer.model = req.body.model;
	offer.start = req.body.start;
	offer.end = req.body.end;
	offer.save(function(err, offer) {
		if (err || !offer) {
		    return res.json({ error : err });
		} else {
		    console.log("Stores" + req.body.stores.split(','));
		    Store.find({"_id": {$in : req.body.stores.split(',')}}, function(err, stores) {
			    console.log("Stores" + stores);
			    for (var i = 0; i < stores.length; i++) {
				stores[i].offers.push(offer.id);
				console.log(offer.id);
				stores[i].save(function(err, store) {
					if (err || !store) {
					    return res.json({error : err});
					}
				    });
			    }
			});
		    return res.json({ success : "Success"});
		}
	    });
    }
};

exports.adminGetOffers = function(Offer) {
    return function(req, res) {
	console.log(req.user.stores);
	Offer.find({stores : {$in : req.user.stores}}, function(err, offers) {
		console.log(offers);
		if (err || !offers) {
		    res.json({ error : err });
		} else {
		    res.send({ "aaData" : offers});
		}
	    });
    }
};

exports.adminGetOffer = function(Admin) {
    return function(req, res) {
	Offer.find({stores : {$in : req.user.stores}, id : req.query.offer_id},
		   function(err, offer) {
		       if (err || !offer) {
			   return res.json({ error : err });
		       } else {
			   return res.json({ offer : offer});
			
}
		   });
    }
};

// TODO actually delete
exports.adminDeleteOffer = function(Admin) {
    return function(req, res) {
	Offer.find({stores : {$in : req.user.stores},
		    id : req.query.offer_id}, function(err, offer) {
		if (err || !offer) {
		    return res.json({ error : err });
		} else {
		    offer.deleted_at = Date.now();
		    offer.save(function(err, offer) {
			    if (err || !offer) {
				return res.json({ error : err });
			    } else {
				return res.json({ offer : offer });
			    }
			});
		}
	    });
    }
};

exports.adminModifyOffer = function(Admin) {
    return function(req, res) {
	Offer.find({stores : {$in : req.user.stores},
		    id : req.query.offer_id}, function(err, offer) {
		if (err || !offer) {
		    return res.json({ error : er });
		} else {
		    // TODO Actually write the code to modify shit here
		    offer.name = req.body.name;
		    offer.save(function(err, offer) {
			    if (err || !offer) {
				return res.json({ error : err });
			    } else {
				return res.json({ offer : offer });
			    }
			});
		}
	    });
    }
};

// Admin functions
exports.signupAdminShow = function() {
    return function(req, res){
	res.render('signup', {
		title: 'Signup',
		countries : CT
	    });
    };
};

exports.updateAdminAccount = function(Admin) {
    return function(req, res) {
	updateAccount({
		name            : req.body.name,
		email           : req.body.email,
		country         : req.body.country,
		pass            : req.body.pass,
	    }, req.user, Admin,
	    function(e, o){
		if (e){
		    res.send(e, 400);
		} else {
		    res.send('ok', 200);
		}
	    });
    }
};

exports.autoLogin = function(Admin) {
    return function(req,res) {
	if (req.isAuthenticated()) {
	    return res.redirect('/');
	} else {
	    return res.render('login', {
		    title: 'Hello - Please Login To Your Account'
		});
	}
    }
};

//Requires req.session.user=o and udata: req.session.user to be passed into accounts.jade which then has an input field, alternatively we can delete by email/username.

exports.delAdminAccount = function(Admin) {
    return function(req,res) {
	Admin.remove({"_id": req.body.id}, function(e, obj){
		if (!e){
		    res.clearCookie('user');
		    res.clearCookie('pass');
                    req.session.destroy(function(e){ res.send('ok', 200); });
		}       else{
		    res.send('record not found', 400);
		}
            });
    }
};

exports.manualLogin = function(Admin) {
    return function(req,res) {
	Admin.findOne({"auth.user":req.body.user}, function(e, o) {
                if (o == null){
		    res.send('User not found',400);
                } else {
		    //res.send(o,200);
		    validatePassword(req.body.pass, o.auth.pass, function(err, ans) {
			    if (ans) {
				req.session.user = o;
                                if (req.body.rememberMe == 'true') {
				    //console.log(req.body.rememberMe);
				    res.cookie('user', o.auth.user, { maxAge: 900000 });
				    //console.log(typeof o.auth.pass);
				    res.cookie('pass', o.auth.pass, { maxAge: 900000 });
                                }
                                res.send(ans, 200);
			    }       else{
				res.send('Invalid password',400);
			    }
			});
		}
	    });
    }
};

//TODO When successful sign up, show a modal and redirect
exports.signupAdmin = function(Admin) {
    return function(req, res) {
        var admin = new Admin();
	Admin.findOne({"auth.user" : req.body.user}, function(e, o) {
		if (o){
		    res.send('Username Taken',400);
		}   else{
		    Admin.findOne({email:req.body.email}, function(e, o) {
			    if (o){
				res.send('Email Taken',400);
			    }       else{
				saltAndHash(req.body.pass, function(hash){
					admin.name = req.body.name;
					admin.post = req.body.post;
					admin.auth.user = req.body.user;
					admin.auth.pass = hash;
					admin.mobile = req.body.mobile;
					admin.email = req.body.email;
					// append date stamp when record was created //       
					admin.create_at = moment().format('MMMM Do YYYY, h:mm:ss a');
					admin.save(function(error, store) {
						if (error || !admin) {
						    res.json({ error : error });
						} else {
						    res.json({ admin : admin });
						}
					    });
				    });
			    }
			});
		}
	    });
    }
};

exports.getTotalClicks = function(Offer) {
    return function(req, res) {
	Offer.findOne({'_id': req.body.id}, function(error, offer) {
		if (error || !offer) {
		    return res.json({ error : error });
		} else {
		    return res.json({ success : offer.click_objects.length });
		}
	    });
    }
};

exports.getTopKUsers = function(Offer) {
    return function(req, res) {
    }
};

exports.getTopKGeo = function(Offer) {
    return function(req, res) {
    }
};

exports.getTopKTimes = function(Offer) {
    return function(req, res) {
    }
};

/* private encryption & validation methods */
    
var updateAccount = function(newData, user, Admin, callback)
{
    Admin.findOne({"_id" : user.id}, function(e, o){
	    o.name          = newData.name;
	    o.email         = newData.email;
	    o.country       = newData.country;
	    if (newData.pass == ''){
		o.save(function(err) {
			if (err) callback(err);
			else callback(null, o);
		    });
	    } else {
		saltAndHash(newData.pass, function(hash){
			o.auth.pass = hash;
			o.save(function(err) {
				if (err) callback(err);
				else callback(null, o);
			    });
		    });
	    }
        });
}

var generateSalt = function() {
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
	    var p = Math.floor(Math.random() * set.length);
	    salt += set[p];
	}
	return salt;
    };

var md5 = function(str) {
    return crypto.createHash('md5').update(str).digest('hex');
};

var saltAndHash = function(pass, callback) {
    var salt = generateSalt();
    callback(salt + md5(pass + salt));
};