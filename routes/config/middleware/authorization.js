var Mongoose = require('mongoose');
var UserSchema = require('../../../models/User.js').UserSchema
    , AdminSchema = require('../../../models/Admin.js').AdminSchema
    , db = Mongoose.createConnection('localhost', 'ConnR')
    , User = db.model('user', UserSchema)
    , Admin = db.model('admin', AdminSchema);

exports.isAuthenticated = function (req, res, next){
    if(req.isAuthenticated()){
        next();
    } else {
        return res.json({error : "Not authenticated"});
    }
};

/* Admin authentication */
exports.isAdminAuthenticated = function (req, res, next) {
    if (req.isAuthenticated() &&
	req.user.permissions != null &&
	req.user.permissions.indexOf("A") >= 0) {
	next();
    } else {
	return res.render('login', {
		title: 'Hello - Please Login To Your Account'
	    });
    }
};

/*
  General permissions

  Required for allowing operations
*/

// B permission
exports.hasControlPanelPermissions = function (req, res, next) {
    if (req.user.permissions != null &&
	req.user.permissions.indexOf("B") >= 0) {
	next();
    } else {
	return res.json({error : "Not allowed"});
    }
};

// C permission
exports.hasOfferPermissions = function (req, res, next) {
    if (req.user.permissions != null &&
	req.user.permissions.indexOf("C") >= 0) {
	next();
    } else {
	return res.json({error : "Not allowed"});
    }
};

// D permission
exports.hasAnalyticsPermissions = function (req, res, next) {
    if (req.user.permissions != null &&
	req.user.permissions.indexOf("D") >= 0) {
	next();
    } else {
	return res.json({error : "Not allowed"});
    }
};

/*
  Operational permissions

  These permissions ensure that all CRUD operations are
  only performed on stores and offers for which the admin
  has ownership
 */
exports.hasStoreAuthorization = function (req, res, next) {
    Admin.findOne({"_id" : req.user.id}, function(err, admin) {
	    console.log(admin);
	    if (err || !admin) {
		return res.json({error : err});
	    } else {
		var req_stores = [];
		if (req.body.stores != null) {
		    req_stores = req.body.stores.split(',');
		} else {
		    req_stores = req.query.stores.split(',');
		}
		console.log(req_stores);
		for (i = 0; i < req_stores.length; i++) {
		    if (admin.stores.indexOf(req_stores[i]) < 0) {
			return res.json({error : "You do not have store authorization"});
		    }	    
		}
		next();
	    }
	});
};

/*
  Admin operational permissions

  These permissions ensure that admins can only affect their
  own subadmins.
 */
exports.hasAdminAuthorization = function (req, res, next) {
    Admin.findOne({"_id" : req.user.id}, function(err, admin) {
	    if (err || !admin) {
		return res.json({error : err});
	    } else {
		if (admin.subadmins.indexOf(req.query.admin_id) >= 0) {
		    next();
		} else {
		    return res.json({error : "You do not have admin privileges"});
		}
	    }
	});
};

exports.hasPermissionAuthorization = function (req, res, next) {
    Admin.findOne({"_id" : req.user.id}, function(err, admin) {
	    if (err || !admin) {
		return res.json({error : err});
	    } else {
		for (i = 0; i < req.body.permissions.length; i++) {
		    if (admin.permissions.indexOf(req.body.permissions[i]) < 0) {
			return res.json({error : "Forbidden"});
		    }
		}
		next();
	    }
	});
};

exports.userExist = function(req, res, next) {
    User.count({
	    email: req.body.email
	}, function (err, count) {
	    if (count === 0) {
		next();
	    } else {
		return res.redirect("/api/user/signup");
	    }
	});
}
